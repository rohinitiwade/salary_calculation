<?php include('header.php'); session_start();
if($_SESSION['user_id']!=''){?>
	<script>
		window.location.href=host+'/index.php';
	</script>
<?php } ?>

<style type="text/css">
	.dataTables_info{
		float: left;
		display: inline-block;
	}
	#savedList_filter{
		float: right;
	}
	.seperator{
		border-top: 2px solid teal;
		width: 100%;
		float: left;
	}
	button.dt-button, div.dt-button, a.dt-button, button.dt-button:focus:not(.disabled), div.dt-button:focus:not(.disabled), a.dt-button:focus:not(.disabled), button.dt-button:active:not(.disabled), button.dt-button.active:not(.disabled), div.dt-button:active:not(.disabled), div.dt-button.active:not(.disabled), a.dt-button:active:not(.disabled), a.dt-button.active:not(.disabled) {
		background-color: #0073aa;
		border-color: #0073aa;
		border-radius: 2px;
		color: #fff;
		background-image: none;
		font-size: 14px;
	}

	div.dt-button-collection button.dt-button:active:not(.disabled), div.dt-button-collection button.dt-button.active:not(.disabled), div.dt-button-collection div.dt-button:active:not(.disabled), div.dt-button-collection div.dt-button.active:not(.disabled), div.dt-button-collection a.dt-button:active:not(.disabled), div.dt-button-collection a.dt-button.active:not(.disabled) {
		background-color: #0073aa;
		border-color: #0073aa;
		background-image: none;
	}
	.p-0 {
		padding: 0;
	}
	.pcoded-content{
		margin-left:0px!important;
	}
	.pcoded .pcoded-container{
		background: white!important;
	}
	.form-control {
		border: 0px;
		border-radius: 0px;
		margin-top: 10px;
		background: transparent;
		margin-bottom: 40px;
		border-bottom: 1px solid #999999;
		-webkit-box-shadow: inset 0 0px 0px rgba(0,0,0,0);
		box-shadow: inset 0 0px 0px rgba(0,0,0,0);
		-webkit-transition: 0.5s;
		-moz-transition: 0.5s;
		transition: 0.5s;
	}
</style>

<div class="pcoded-content">
	<div class="pcoded-inner-content">

		<!-- Main-body start -->
		<div class="main-body">
			<div class="page-wrapper">
				<!-- Page-header start -->
				<div class="page-header">
					<div class="page-header-title">
						<h4>Salary Calculation</h4>
					</div>

				</div>
				<!-- Page-header end -->
				<!-- Page-body start -->
				<div class="page-body">
					<div class="col-sm-12">
						<div class="col-sm-3 col-sm-offset-2 wow fadeInUp" data-wow-delay="0s">
							<label><b style="color: red">Establishment (Company) Name</b></label>
							<select class="form-control" id="establishment_name">
								<option>--Select --</option>

							</select>
						</div>
						<div class="col-sm-3">
							<label>Location Name</label>
							<select class="form-control" id="location" onchange="getLocation()"></select>
						</div>
						<div class="col-sm-3 wow fadeInUp" data-wow-delay="0s">
							<label>Salary Month</label>
							<select class="form-control" id="salary_month">
								<option value="0">--Select --</option>
								<option value="1">January</option>
								<option value="2">February</option>
								<option value="3">March</option>
								<option value="4">April</option>
								<option value="5">May</option>
								<option value="6">June</option>
								<option value="7">July</option>
								<option value="8">August</option>
								<option value="9">September</option>
								<option value="10">October</option>
								<option value="11">November</option>
								<option value="12">December</option>
							</select>
						</div>
					</div>

					<div class="col-sm-3 wow fadeInUp" data-wow-delay="0s">
						<label>Employee Name</label>
						<select class="form-control" id="employee_name" onchange="getEmployeeName()">
							<option>--Select --</option>

						</select>
					</div>
					<div class="col-sm-3 wow fadeInUp" data-wow-delay="0.2s">
						<label>Payable Days</label>
						<input type="text" id="payable_days" name="" class="form-control">
					</div>
					<div class="col-sm-3 wow fadeInUp" data-wow-delay="0.4s">
						<label>Over Time/ Hour(from structural form)</label>
						<input type="text" id="over_time_structural" name="" class="form-control" disabled>
					</div>
					<div class="col-sm-3 wow fadeInUp" data-wow-delay="0.4s">
						<label>Over Time/ Hour</label>
						<input type="text" id="over_time" name="" class="form-control">
					</div>
					<div class="col-sm-3 wow fadeInUp" data-wow-delay="0.4s">
						<label>Incentives</label>
						<input type="text" id="incentive" class="form-control" name="">
					</div>
					<div class="col-sm-3 wow fadeInUp" data-wow-delay="0.4s">
						<label>Transport Alowance</label>
						<input type="text" id="transport_allowances" class="form-control" name="">
					</div>
					<div class="col-sm-3 wow fadeInUp" data-wow-delay="0.4s">
						<label>Loan</label>
						<input type="text" id="loan" class="form-control" name="">
					</div>
					<div class="col-sm-3 wow fadeInUp" data-wow-delay="0.4s">
						<label>Advance</label>
						<input type="text" id="advance" class="form-control" name="">
					</div>
					<div class="col-sm-3 wow fadeInUp" data-wow-delay="0.4s">
						<label>Total Salary</label>
						<input type="text" id="total_salary" class="form-control" name="">
					</div>
					<div class="col-sm-7 form-group">
						<button type="button" class="btn btn-info btn-sm"  id="edit_salary" onclick="submitValue()">Calculate Salary</button>
						<button type="button" class="btn btn-info btn-sm"  id="update_salary" onclick="updateSalary()">Update Salary</button>
						<button type="button" class="btn btn-warning btn-sm" onclick="salarySlip()">Calculate Salary Slip</button>
						<button type="button" class="btn btn-primary btn-sm" onclick="pfChallan()" id="pf-challan-btn">Calculate PF Challan</button>
						<button type="button" class="btn btn-success btn-sm" onclick="esicChallan()" id="esic-challan-btn">Calculate ESIC Challan</button>
					</div>
					<div class="col-sm-12 form-group">
						<ul class="nav nav-tabs md-tabs" role="tablist">
							<li class="nav-item">
								<a class="nav-link active" data-toggle="tab" href="#caluate-salary" role="tab">Calculate Salary</a>
								<div class="slide"></div>
							</li>
							<li class="nav-item">
								<a class="nav-link" data-toggle="tab" href="#salary-slip" role="tab">Calculate Salary Slip</a>
								<div class="slide"></div>
							</li>
							<li class="nav-item" id="pf-challan-li">
								<a class="nav-link" data-toggle="tab" href="#pf-challan" role="tab">Calculate PF Challan</a>
								<div class="slide"></div>
							</li>
							<li class="nav-item" id="esic-challan-li">
								<a class="nav-link" data-toggle="tab" href="#esic-challan" role="tab">Calculate ESIC Challan</a>
								<div class="slide"></div>
							</li>                                                                                                                                                                         </ul>
						</div>
						<hr class="seperator">
						<input type="hidden" id="structure_basic" name="">
						<input type="hidden" id="structure_da" name="">
						<input type="hidden" id="structure_hra" name="">
						<input type="hidden" id="structure_conv" name="">
						<input type="hidden" id="structure_esic_no" name="">
						<input type="hidden" id="structure_establishment" name="">
						<input type="hidden" id="structure_location" name="">
						<input type="hidden" id="structure_gender" name="">
						<input type="hidden" id="structure_medical" name="">
						<input type="hidden" id="structure_overtime" name="">
						<input type="hidden" id="structure_personalpay" name="">
						<input type="hidden" id="structure_pf12" name="">
						<input type="hidden" id="structure_pf8" name="">
						<input type="hidden" id="structure_revised_month" name="">
						<input type="hidden" id="structure_salary_fixed" name="">
						<input type="hidden" id="structure_uan" name="">
						<input type="hidden" id="structure_strid" name="">
						<input type="hidden" id="structure_regid" name="">
						<input type="hidden" id="new_empid" name="">
						<input type="hidden" id="structure_emp_name" name="">
						<input type="hidden" id="fixed_pf12" name="">
						<input type="hidden" id="fixed_pf8" name="">
						<input type="hidden" id="fixed_pf3" name="">
						<input type="hidden" id="fixed_esic" name="">
						<input type="hidden" id="fixed_esic1" name="">
						<div class="card-block tab-content">
							<div class="tab-pane show active" id="caluate-salary" role="tabpanel">
								<div class="table-responsive col-sm-12" id="savedList"> </div>
							</div>
							<div class="tab-pane " id="salary-slip" role="tabpanel">
<div class="content-footer" style="float:left"><button class="btn btn-info btn-mini" onclick="exportHTML()">Export</button></div>
<div id="salary_table_list"></div>
							</div>
							<div class="tab-pane " id="pf-challan" role="tabpanel">
								<div class="table-responsive col-sm-12" id="pf-challan-list"> </div>
							</div>
							<div class="tab-pane " id="esic-challan" role="tabpanel">
								<div class="table-responsive col-sm-12" id="esic-challan-list"> </div>
							</div>
						</div>
					</div>
				</div>
				<!-- Page-body end -->
			</div>
		</div>
		<!-- Main-body end -->


	</div>
</div>




<!-- Jquery and Js Plugins -->

<?php include('footer.php') ?>
<!-- <script type="text/javascript" src="assets/js/connectionutils.js"></script>
<script type="text/javascript" src="assets/js/toastr.min.js"></script> -->
<script type="text/javascript" src="assets/js/calculate_salary.js"></script>

