<?php include('header.php'); session_start();   $_SESSION['user_id'];
if($_SESSION['user_id']!=''){?>
  <script>
    window.location.href=host+'/index.php';
  </script>
<?php } ?>
<!DOCTYPE html>
  <style type="text/css">
    .dataTables_info{
      float: left;
      display: inline-block;
    }
    #structural_table_filter{
      float: right;
    }
   
    .checkbox-fade label {
      line-height: 25px;
    }
    .structural-content{
      padding-top: 3%;
    }
    .checkbox-fade{
      margin-right: 0px;
    }
    .checkbox-fade.fade-in-primary .cr {
      border: 2px solid #0073aa;
    }
    .checkbox-fade .cr {
      border-radius: 0;
      border: 2px solid #0073aa;
      cursor: pointer;
      display: inline-block;
      float: left;
      height: 20px;
      margin-right: .5em;
      position: relative;
      width: 20px;
    }
    .checkbox-fade label {
      line-height: 25px;
    }
    .checkbox-fade label:after {
      content: '';
      display: table;
      clear: both;
    }
    .pcoded-content{
    margin-left:0px!important;
  }
  .pcoded .pcoded-container{
    background: white!important;
  }
  .form-control {
    border: 0px;
    border-radius: 0px;
    margin-top: 10px;
    background: transparent;
    margin-bottom: 40px;
    border-bottom: 1px solid #999999;
    -webkit-box-shadow: inset 0 0px 0px rgba(0,0,0,0);
    box-shadow: inset 0 0px 0px rgba(0,0,0,0);
    -webkit-transition: 0.5s;
    -moz-transition: 0.5s;
    transition: 0.5s;
  }
  </style>

 
<div class="pcoded-content">
<div class="pcoded-inner-content">

<!-- Main-body start -->
<div class="main-body">
<div class="page-wrapper">
<!-- Page-header start -->
<div class="page-header">
<div class="page-header-title">
<h4>Salary Calculation</h4>
</div>

</div>
<!-- Page-header end -->
<!-- Page-body start -->
<div class="page-body">
    <div class="services-section text-center" id="fixed_values">
      <div class="container">
        <div class="col-md-8 col-md-offset-2 nopadding">
          <div class="services-content">
            <h1 class="wow fadeInUp" data-wow-delay="0s">Please Provide following.</h1>
            <p>This form is to be used for entering fixed values of ESIC, PF.</p>
          </div>
          <div>
          </div>

        </div>
        <form method="POST" id="pf_table" class="table-responsive col-sm-12 p-0">
          <table class="table table-bordered" id="fixed_values_table">
            <thead>
              <tr>
                <th>ESIC</th>
                <th>ESIC</th>
                <th>PF (12%)</th>
                <th>PF (8.33%)</th>
                <th>PF (3.67%)</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              <tr>
               <!--  <td><input type="text" class="form-control" placeholder="Enter ESIC" id="fixed-esic" name="esic_no">
                </td>
                 <td><input type="text" class="form-control" placeholder="Enter ESIC" id="fixed-esic" name="esic_no">
                </td>
                <td><input type="text" class="form-control" placeholder="Enter PF (12%)" id="fixed-pf-12" name="pf1"></td>
                <td><input type="text" class="form-control" placeholder="Enter PF (8.33%)" id="fixed-pf-8" name="pf2"></td>
                <td><input type="text" class="form-control" placeholder="Enter PF (3.67%)" id="fixed-pf-3" name="pf3"></td>
                <td><button class="btn btn-success btn-mini" type="button" onclick="save_updateFixedValues('Save')">Save</button>
                  <button class="btn btn-success btn-mini" type="button" onclick="save_updateFixedValues('Edit')" id="updateFixed_val">Update</button></td> -->
                </tr>
              </tbody>
            </table>
          </div>
        </div>
        <div class="flex-features text-center" id="form"><!-- Services section (small) with icons -->
          <div class="container">
            <div class="col-md-8 col-md-offset-2 nopadding">
              <div class="services-content">
                <h1 class="wow fadeInUp" data-wow-delay="0s">Please Provide following Structural values.</h1>
                <p>This form is to be used for entering employee's basic, da,hra,conv etc. for salary calculation.This data to be entered once for every user,till the values are changed</p>
              </div>
            </div>
            <form method="POST" id="structual_id">
<!-- <div class="col-sm-3 wow fadeInUp" data-wow-delay="0s">
            <label>Establishment (Company) Name</label>
            <input type="text" id="basic" name="" class="form-control">
          </div> -->
          <input type="hidden" id="emp_id" name="">
          <div class="col-sm-12">
           <div class="col-sm-3 wow fadeInUp col-sm-offset-3" data-wow-delay="0.2s">
            <label style="color: red;font-weight: bold;">Establishment (Company) Name <span style="color: red">*</span></label>
            <textarea class="form-control" id="establishment_name" rows="1" ></textarea>
          </div>  
          <div class="col-sm-3 wow fadeInUp" data-wow-delay="0.2s">
            <label style="color: red;font-weight: bold;">Establishment Location </label>
            <input type="text" class="form-control" id="est_location" name="">
          </div>
        </div>
        <div class="col-sm-12">

          <div class="col-sm-3 wow fadeInUp" data-wow-delay="0.2s">
            <label>UAN No. </label>
            <input type="text" id="uan_no" name="" maxlength="12" minlength="12" class="form-control" onkeypress="return isNumber(event)">
          </div>
          <div class="col-sm-3 wow fadeInUp" data-wow-delay="0.2s">
            <label>ESIC No. </label>
            <input type="text" id="esic_no" name="" class="form-control" onkeypress="return isNumber(event)">
          </div>
          <div class="col-sm-3 wow fadeInUp" data-wow-delay="0.2s">
            <label>Employee Name <span style="color: red">*</span></label>
            <input type="text" id="emp_name" name="" class="form-control">
          </div>
            <div class="col-sm-3 wow fadeInUp" data-wow-delay="0.2s">
            <label>Account Number </label>
            <input type="text" class="form-control" id="account_number" name="">
          </div>
        </div>

        <div class="col-sm-12">
          <div class="col-sm-3 wow fadeInUp" data-wow-delay="0.2s">
            <label>Gender <span style="color: red">*</span></label>
            <select class="form-control" id="gender">
              <option value="">-- Select --</option>
              <option value="Male">Male</option>
              <option value="Female">Female</option>
            </select>
          </div>
          <div class="col-sm-3 wow fadeInUp" data-wow-delay="0.2s">
            <label>Basic <span style="color: red">*</span></label>
            <input type="text" id="basic" name="" class="form-control" onkeypress="return isNumber(event)">
          </div>
          <div class="col-sm-3 wow fadeInUp" data-wow-delay="0.2s">
            <label>DA <span style="color: red">*</span></label>
            <input type="text" id="da" name="" class="form-control" onkeyup="getHRA()" onkeypress="return isNumber(event)">
          </div>
          <div class="col-sm-3 wow fadeInUp" data-wow-delay="0.2s">
            <label>HRA <span style="color: red">*</span></label>
            <input type="text" id="hra" name="" class="form-control" onkeypress="return isNumber(event)">
          </div>
         
        </div>

        <div class="col-sm-12">
          <div class="col-sm-3 wow fadeInUp" data-wow-delay="0.2s">
            <label>Conv. <span style="color: red">*</span></label>
            <input type="text" id="conv" name="" class="form-control" onkeypress="return isNumber(event)">
          </div>
          <div class="col-sm-3  wow fadeInUp" data-wow-delay="0.2s">
            <label>Medical Allowance</label>
            <input type="text" id="medical_allowance" name="" class="form-control" onkeypress="return isNumber(event)">
          </div>
          <!-- <div class="col-sm-3 wow fadeInUp" data-wow-delay="1.2s">
            <label>Incentives</label>
            <input type="text" id="incentives" name="" class="form-control">
          </div> -->
          <div class="col-sm-3 wow fadeInUp" data-wow-delay="0.2s">
            <label>Over Time Wages Per Hour / Per Day</label>
            <input type="text" id="over_time" name="" class="form-control" onkeypress="return isNumber(event)">
          </div>
          <div class="col-sm-3 wow fadeInUp" data-wow-delay="0.2s">
            <label>Personal Pay</label>
            <input type="text" id="personal_pay" name="" class="form-control">
          </div>
         

        </div>
        <div class="col-sm-12 form-group">
          <div class="col-sm-3 checkbox-fade fade-in-primary wow fadeInUp" data-wow-delay="0.2s">
            <label>
              <input type="checkbox" value="" name="pf_salary_12">
              <span class="cr">
                <i class="cr-icon icofont icofont-ui-check txt-primary"></i>
              </span> <span> Pf(12%) deduction on 15K</span>
            </label>
            <!-- <label><input type="checkbox" name="pf_salary"></label> -->
          </div>
          <div class="col-sm-3 checkbox-fade fade-in-primary wow fadeInUp" data-wow-delay="0.2s">
            <label>
              <input type="checkbox" value="" name="pf_salary_8">
              <span class="cr">
                <i class="cr-icon icofont icofont-ui-check txt-primary"></i>
              </span> <span> Pf(8.33%) deduction on 15K</span>
            </label>
            <!-- <label><input type="checkbox" name="pf_salary"></label> -->
          </div>
          <div class="col-sm-3 checkbox-fade fade-in-info wow fadeInUp" data-wow-delay="0.2s">
           <label>
            <input type="checkbox" value="" name="salary_month_days" id="salary_check">
            <span class="cr">
              <i class="cr-icon icofont icofont-ui-check txt-info"></i>
            </span> <span> Salary on Fixed Month Days</span>
          </label>
          <!-- <label><input type="checkbox" name="salary_month_days" id="salary_check"> Salary on Fixed Month Days</label> -->
        </div>
        <div class="col-sm-3" id="revised_month_div">
          <label>Revised Month Days</label>
          <input class="form-control" id="revised_month_days" name="" onkeypress="return isNumber(event)">
        </div>
       
      </div>    
      <div class="col-sm-12 text-center">
        <button type="button" class="btn btn-info btn-sm" onclick="submitValue()" id="submit_structure">Submit</button>
        <button  type="button" class="btn btn-info btn-sm" onclick="updateEntry()" id="update_structure">Update</button>
      </div>

    </form>
  </div>
</div>

<div class="services-section text-center">
  <div class=" structural-content">
    <div class=" table-responsive col-sm-12 " id="savedList">
    </div>
  </div>  
</div>

<!-- Scroll To Top -->

<a id="back-top" class="back-to-top page-scroll" href="#main">
  <i class="ion-ios-arrow-thin-up"></i>
</a>

<!-- Scroll To Top Ends-->


</div><!-- Main Section -->
</div><!-- Wrapper-->

<!-- Jquery and Js Plugins -->
<?php include('footer.php') ?>
<script type="text/javascript" src="assets/js/connectionutils.js"></script>
<script type="text/javascript" src="assets/js/index.js"></script>
