function submitValue(){
	var total_month_days;
	var month_selected = $("#salary_month option:selected").val();
	if(month_selected == "0")
		alert("Please select Salary month");
	else if(month_selected == '1')
		total_month_days = 31;
	else if(month_selected== '2'){
		var currentmonth = leapYear(new Date().getFullYear());
		if(!currentmonth)
			total_month_days = 28;
		else
			total_month_days = 29;
	}
	else if(month_selected == '3')
		total_month_days = 31;
	else if(month_selected == '4')
		total_month_days = 30;
	else if(month_selected == '5')
		total_month_days = 31;
	else if(month_selected == '6')
		total_month_days = 30;
	else if(month_selected == '7')
		total_month_days = 31;
	else if(month_selected == '8')
		total_month_days = 31;
	else if(month_selected == '9')
		total_month_days = 30;
	else if(month_selected == '10')
		total_month_days = 31;
	else if(month_selected == '11')
		total_month_days = 30;
	else if(month_selected == '12')
		total_month_days = 31;

	var basic = parseInt($("#basic").val());
	var da = parseInt($("#da").val());
	var conv = parseInt($("#conv").val());
	var hra = parseInt($("#hra").val());
	var medical = parseInt($("#medical_allowance").val());
	var overtime = parseInt($("#over_time").val());
	var personal_pay = parseInt($("#personal_pay").val());
	var fixed_pf12 = 12;
	var fixed_pf8 = 8.33;
	var fixed_esic = 0.75;
	var fixed_esic2 = 3.25;
	var transport_allowances = parseInt($("#transport_allowances").val());
	// var est_name = $("#establishment_name option:selected").val();
	// var location = $("#location option:selected").val();
	// var basic = 4500,da=1401,hra=500,conv=100;
	if($("#payable_days").val() == ''){
		alert("Please provide payable days to calculate salary");
		return false;
	}
	else if($("#payable_days").val() > total_month_days){
		alert("Payable days cannot be greater than total month days");
		return false;
	}
	else
		var payable_days = parseInt($("#payable_days").val());

	var new_basic,new_da,new_hra,new_conv,pf_2;

	new_basic = Math.round((basic/total_month_days)*payable_days);
	new_da = Math.round((da/total_month_days)*payable_days);
	new_hra = Math.round((hra/total_month_days)*payable_days);
	new_conv = Math.round((conv/total_month_days)*payable_days);
	
	var gross = Math.round(new_basic+new_da+new_hra+new_conv + medical + overtime + personal_pay + transport_allowances);

	if(gross < 21000 || over_time !='')
		var esic =	Math.ceil((gross * fixed_esic) / 100) ;
	else
		var esic = 0;

	if(gross <= 7500)
		var pt = 0;
	else if(gross > 7500 && gross <= 10000 && gender == 'M')
		var pt = 175;
	else if (gross > 7500 && gross <= 10000 && gender == 'F')
		var pt = 0;
	else
		var pt = 200;

	var pf = Math.round(((basic + da) * fixed_pf12)/100);

	var net = gross - esic - pt - pf;

	pf_2 = Math.round(((basic + da) * fixed_pf8)/100);

	var pf_3 = Math.round(pf - pf_2);

	if(gross < 21000)
		var esic_2 = Math.round((gross * fixed_esic2 )/ 100);
	else
		var esic_2 = 0;

	var payment = gross - esic - pt - pf - pf_2 - pf_3 - esic_2;

	var ctc = Math.round(basic + da + hra + conv + pf + esic_2);
	var salary_tr = '<tr>';
	salary_tr += '<td>1</td>';
	salary_tr += '<td>ABC</td>';
	salary_tr += '<td>' + payable_days +'</td>';
	salary_tr += '<td>' + total_month_days+'</td>';
	salary_tr += '<td>' + new_basic +'</td>';
	salary_tr += '<td>' + new_da +'</td>';
	salary_tr += '<td>' + new_hra +'</td>';
	salary_tr += '<td>' + new_conv +'</td>';
	salary_tr += '<td>' + gross+'</td>';
	salary_tr += '<td>' + esic+'</td>';
	salary_tr += '<td>' + pt+'</td>';
	salary_tr += '<td>' + pf+'</td>';
	salary_tr += '<td>' + net+'</td>';
	salary_tr += '<td>' + pf_2+'</td>';
	salary_tr += '<td>' + pf_3+'</td>';
	salary_tr += '<td>' + esic_2+'</td>';
	salary_tr += '<td>' + payment+'</td>';
	salary_tr += '<td>' + ctc+'</td></tr>';

	var salary_table = '<table class="table table-bordered"><thead>';
	salary_table += '<tr><th>Sr. No</th>';
	salary_table += '<th>Name</th>';
	salary_table += '<th>Payable Days</th>';
	salary_table += '<th>Month Format</th>';
	salary_table += '<th>Basic </th>';
	salary_table += '<th>DA</th>';
	salary_table += '<th>HRA</th>';
	salary_table += '<th>Conv</th>';
	salary_table += '<th>Gross</th>';
	salary_table += '<th>ESIC <span>('+fixed_esic+')</span></th>';
	salary_table += '<th>PT</th>';
	salary_table += '<th>PF <span>('+fixed_pf12+')</span></th>';
	salary_table += '<th>Net Earning</th>';
	salary_table += '<th>PF <span>('+fixed_pf8+')</span></th>';
	salary_table += '<th>PF <span>(3.67)</span></th>';
	salary_table += '<th>ESIC <span>('+fixed_esic2+')</span></th>';
	salary_table += '<th>Payment</th>';
	salary_table += '<th>CTC</th></tr></thead><tbody>';
	salary_table += salary_tr;
	salary_table += '</tbody></table>';

	$("#savedList").html(salary_table);

}