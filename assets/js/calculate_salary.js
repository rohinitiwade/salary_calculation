/**
 * 
 */

 loginType = '';
 $(document).ready(function(){
 	$("#update_salary").hide();
 	loginType = localStorage.getItem('logintype');
 	if(loginType == 'user'){
 		$("#pf-challan-li").hide();
 		$("#esic-challan-li").hide();
 		$("#esic-challan-btn").hide();
 		$("#pf-challan-btn").hide();
 		$("#structure_fixed").hide();
 		$("#structure_form").hide();
 	}
 	else{
 		$("#pf-challan-li").show();
 		$("#esic-challan-li").show();
 		$("#esic-challan-btn").show();
 		$("#pf-challan-btn").show();
 		$("#structure_fixed").show();
 		$("#structure_form").show();
 	}

 });
 getEstablishmentName();

 function getEstablishmentName(){
 	$.ajax({
 		url:host+'/company_data.php',
 		dataType:'json',
 		type : 'GET',
 		data : {
 			value : 'est'
 		},
 		contextType:'application/json',
 		success:function(response){
 			console.log(response);
 			var company_option = '<option>-- Select --</option>'
 			$.each(response,function(i,obj){
 				company_option+='<option>'+obj.companyname+'</option>';
 			});
 			$("#establishment_name").html(company_option);
 		},
 		error: function(){
 			toastr.error("Error in getting establishment name",{timeout:5000});
 		}
 	});
 }

 $(document).ready(function(){
 	$("#establishment_name").on('change',function(){
 		var val = $(this).val();
 		var estVo={
 			establishment : val,
 			value :'location'
 		}
 		$.ajax({
 			url:host+'/location.php',
 			dataType:'json',
 			type : 'POST',
 			data : JSON.stringify(estVo),
 			contextType:'application/json',
 			success:function(response){
 				console.log(response);
 				var location_option = '<option value="">-- Select --</option>'
 				$.each(response,function(i,obj){
 					location_option+='<option>'+obj.est_location+'</option>';
 				});
 				$("#location").html(location_option);
 				getLocation();
 			},
 			error: function(){
 				toastr.error("Error in getting location name",{timeout:5000});
 			}
 		});
 	});
 	$("#salary_month").on('change',function(){	
 		getCalculteSalaryData();	
 	});
 });
 function getLocation(){
 	var val = $("#location").val();
 	var establishment = $("#establishment_name").val();
 	var estVo={
 		establishment : establishment,
 		location :val
 	}
 	$.ajax({
 		url:host+'/emp_namedata.php',
 		dataType:'json',
 		type : 'POST',
 		data : JSON.stringify(estVo),
 		contextType:'application/json',
 		success:function(response){
 			console.log(response);
 			var emp_option = '<option>-- Select --</option>'
 			$.each(response,function(i,obj){
 				emp_option+='<option value="'+obj.str_id+'">'+obj.emp_name+'</option>';
 			});
 			$("#employee_name").html(emp_option);
 		},
 		error: function(){
 			toastr.error("Error in getting employee name",{timeout:5000});
 		}
 	});
 }
 function getEmployeeName(){
 	var val = $("#employee_name").val();
 	var location=$("#location").val();
 	var establishment = $("#establishment_name").val();
 	var transport_allowances  = $("#transport_allowances").val();
 	var empVo={
 		emp_name : val,
 		establishment : establishment
 	}
 	$.ajax({
 		url:host+'/emp_structuredata.php',
 		dataType:'json',
 		type : 'POST',
 		data : JSON.stringify(empVo),
 		contextType:'application/json,utf-8',
 		success:function(response){
 			console.log(response);
 			$("#structure_basic").val(response.basic);
 			$("#structure_da").val(response.da);
 			$("#structure_conv").val(response.conv);
 			$("#structure_hra").val(response.hra);
 			$("#structure_esic_no").val(response.esic_no);
 			$("#structure_uan").val(response.uan_no);
 			$("#structure_establishment").val(response.establishment);
 			$("#structure_location").val(response.est_location);
 			$("#structure_gender").val(response.gender);
 			$("#structure_medical").val(response.medical_allowance);
 			$("#structure_personalpay").val(response.personal_pay);
 			$("#structure_pf8").val(response.pfdeduction_eight);
 			$("#structure_pf12").val(response.pfdeduction_twelve);
 			$("#structure_revised_month").val(response.revised_month);
 			$("#structure_salary_fixed").val(response.salary_fixedmonth);
 			$("#structure_strid").val(response.str_id);
 			$("#structure_personalpay").val(response.personal_pay);
 			$("#fixed_esic").val(response.esic);
 			$("#fixed_esic1").val(response.esic_two);
 			$("#fixed_pf12").val(response.pf_one);
 			$("#fixed_pf8").val(response.pf_two);
 			$("#fixed_pf3").val(response.pf_three);
 			$("#over_time_structural").val(response.over_time);
			// $("#structure_emp_name").val()
		},
		error: function(){
			toastr.error("Error in getting structure data",{timeout:5000});
		}
	});
 }

 function leapYear(year)
 {
 	return ((year % 4 == 0) && (year % 100 != 0)) || (year % 400 == 0);
 }
 function isEmpty(val) {
 	return (val === undefined || val == null || val == "null" || val.length <= 0) ? true : false;
 }
 function submitValue(){
 	calculationSalary();

 	getSalaryTable();
 }
 var month_selected,establishment,location,over_time,emp_id,payable_days,total_month_days,new_basic,new_da,new_hra,new_conv,transport_allowances,gross,esic,pt,pf,net,pf_2,pf_3,esic_2,payment,ctc,incentives,loan,advance;
 function calculationSalary(){
 	incentives = $("#incentive").val();

 	total_month_days;
 	month_selected = $("#salary_month option:selected").val();
 	if(month_selected == "0")
 		alert("Please select Salary month");
 	else if(month_selected == '1')
 		total_month_days = 31;
 	else if(month_selected== '2'){
 		var currentmonth = leapYear(new Date().getFullYear());
 		if(!currentmonth)
 			total_month_days = 28;
 		else
 			total_month_days = 29;
 	}
 	else if(month_selected == '3')
 		total_month_days = 31;
 	else if(month_selected == '4')
 		total_month_days = 30;
 	else if(month_selected == '5')
 		total_month_days = 31;
 	else if(month_selected == '6')
 		total_month_days = 30;
 	else if(month_selected == '7')
 		total_month_days = 31;
 	else if(month_selected == '8')
 		total_month_days = 31;
 	else if(month_selected == '9')
 		total_month_days = 30;
 	else if(month_selected == '10')
 		total_month_days = 31;
 	else if(month_selected == '11')
 		total_month_days = 30;
 	else if(month_selected == '12')
 		total_month_days = 31;

 	var basic = parseInt($("#structure_basic").val());
 	var da = parseInt($("#structure_da").val());
 	if(isEmpty($("#structure_conv").val()))
 		var conv = 0;
 	else
 		var conv = parseInt($("#structure_conv").val());
 	var hra = parseInt($("#structure_hra").val());
 	var esic_no = $("#structure_esic_no").val();
 	var uan_no = $("#structure_uan").val();
 	var establishment =$("#structure_establishment").val();
 	var location = $("#structure_location").val();
 	var gender = $("#structure_gender").val();
 	var structure_overtime = $("#over_time_structural").val();
 	var pf8 = parseInt($("#structure_pf8").val());
 	var pf12 = parseInt($("#structure_pf12").val());
 	var revised = $("#structure_revised_month").val();
 	var fixedmonth = $("#structure_salary_fixed").val();
 	emp_id = $("#structure_strid").val();
 	var emp_name = $("#employee_name option:selected").text();
 	var fixed_pf12 = $("#fixed_pf12").val();
 	var fixed_pf8 = $("#fixed_pf8").val();
 	var fixed_esic = $("#fixed_esic").val();
 	var fixed_esic1 = $("#fixed_esic1").val();

 	if(!isEmpty( $("#loan").val()))
 		loan = parseInt( $("#loan").val());
 	else
 		loan = 0;

 	if(!isEmpty($("#advance").val()))
 		advance =  parseInt($("#advance").val());
 	else
 		advance = 0;
 	if(!isEmpty($("#over_time").val()) && !isEmpty(structure_overtime))
 		over_time =  parseInt($("#over_time").val()) * parseInt(structure_overtime);
 	else
 		over_time = 0;

 	if(!isEmpty($("#transport_allowances").val()))
 		transport_allowances = parseInt($("#transport_allowances").val());
 	else
 		transport_allowances =0;

 	if(!isEmpty($("#structure_personalpay").val()))
 		personal_pay = parseInt($("#structure_personalpay").val());
 	else
 		personal_pay = 0;
 	if (!isEmpty($("#structure_medical").val()))
 		medical = parseInt($("#structure_medical").val());
 	else
 		medical = 0;

 	var total_salary = $("#total_salary").val();

 	payable_days = $("#payable_days").val();
 	if(isEmpty(payable_days)){
 		toastr.warning('Please provide payable days of selected employee',{timeout : 5000});
 		return false;
 	}
 	else if((!isEmpty(revised) && revised != "0") && payable_days > revised){
 		toastr.error($("#structure_revised_month").val(),"Please select payable days less than or equal to",{timeout:5000});
 		return false;
 	}
 	else if(!isEmpty(payable_days) && payable_days > total_month_days){
 		toastr.error(total_month_days,"Please select payable days less than or equal to",{timeout:5000});
 		return false;
 	}
	// var new_basic,new_da,new_hra,new_conv,pf_2;
	if(fixedmonth == 1){
		new_basic = Math.round((basic/revised)*payable_days);
		new_da = Math.round((da/revised)*payable_days);
		new_hra = Math.round((hra/revised)*payable_days);
		new_conv = Math.round((conv/revised)*payable_days);
	}
	else{
		new_basic = Math.round((basic/total_month_days)*payable_days);
		new_da = Math.round((da/total_month_days)*payable_days);
		new_hra = Math.round((hra/total_month_days)*payable_days);
		new_conv = Math.round((conv/total_month_days)*payable_days);
	}

	gross = Math.round(new_basic+new_da+new_hra+new_conv + medical + over_time + personal_pay + transport_allowances);

	if(!isEmpty(total_salary)){
		if(gross != total_salary){
			toastr.info("Generated salary is not equal to the total salary of employee");
			return false;
		}
	}

	if(gross < 21000 || over_time !='' || incentives !='')
		esic =Math.ceil((gross * fixed_esic) / 100) ;
	else
		esic = 0;

	if(gross <= 7500)
		pt = 0;
	else if(gross > 7500 && gross <= 10000 && gender == 'Male')
		pt = 175;
	else if (gross > 7500 && gross <= 10000 && gender == 'Female')
		pt = 0;
	else
		pt = 200;


	if((basic + da) > 15000 && pf12 == '1')
		pf = Math.round((15000 *fixed_pf12) /100);
	else
		pf = Math.round(((new_basic + new_da) * fixed_pf12)/100);
	net = gross - esic - pt - pf - loan - advance;

	if(pf8 == '1')
		pf_2 = Math.round((15000 * fixed_pf8)/100);
	else
		pf_2 = Math.round(((new_basic + new_da) * fixed_pf8)/100);

	pf_3 = Math.round(pf - pf_2);

	if(gross < 21000)
		esic_2 = Math.round((gross * fixed_esic1 )/ 100);
	else
		esic_2 = 0;

	payment = gross - esic - pt - pf - pf_2 - pf_3 - esic_2;

	ctc = Math.round(basic + da + hra + conv + pf + esic_2);
}

function getSalaryTable(){
	var empVO = new Object();
	empVO.salary_month = month_selected;
	empVO.emp_id = emp_id;
	empVO.overtime = over_time;
	empVO.payable_days = payable_days;
	empVO.total_month_days = total_month_days;
	empVO.new_basic = new_basic;
	empVO.new_da = new_da;
	empVO.new_hra = new_hra;
	empVO.new_conv = new_conv;
	empVO.overtime = over_time;
	empVO.transport_allowances = transport_allowances;
	empVO.gross = gross;
	empVO.esic = esic;
	empVO.pt = pt;
	empVO.pf = pf;
	empVO.net = net;
	empVO.pf_2 = pf_2;
	empVO.pf_3 = pf_3;
	empVO.esic_2 = esic_2;
	empVO.payment = payment;
	empVO.ctc = ctc;
	empVO.loan = loan;
	empVO.advance = advance;
	empVO.incentives = incentives;

	var emp_name = $("#employee_name option:selected").text();

	$.ajax({
		url:host+'/submit_new_structural.php',
		dataType:'json',
		type : 'POST',
		data : JSON.stringify(empVO),
		contextType:'application/json,utf-8',
		success:function(response){
			getCalculteSalaryData();
		},
		error: function(){
			toastr.error("Error in generating salary of " +emp_name,{timeout:5000});
		}
	});
}

function getCalculteSalaryData(){
	var counter = 0;
	var salarymonth = $("#salary_month").val();
	var establishment = $("#establishment_name").val();
	var location = $("#location").val();
	var salaryVO = new Object();
	salaryVO.establishment_name = establishment;
	salaryVO.est_location = location;
	salaryVO.salary_month = salarymonth;

	$.ajax({
		url:host+'/alldatafetch.php',
		dataType:'json',
		type : 'POST',
		data : JSON.stringify(salaryVO),
		contextType : 'application/json,utf-8',
		success:function(response){
			$("#salary-slip-li").removeClass('active');
			$("#salary-calculation-li").addClass('active');
			$("#pf-challan-li").removeClass('active');
			$("#esic-challan-li").removeClass('active');
			console.log("FINAL RESPOMNSE",response);
			var salary_tr = '',fixed_esic,fixed_pf12,fixed_pf8,fixed_pf3,fixed_esic2;
			$.each(response,function(i,obj){
				fixed_pf12 = obj.pf_one;
				fixed_pf8 = obj.pf_two;
				fixed_pf3 = obj.pf_three;
				fixed_esic = obj.esic;
				fixed_esic2 = obj.esic_two;
				counter++;
				salary_tr += '<tr>';
				salary_tr += '<td><input type="hidden" id="employeeData-'+obj.str_id+'" value="'+obj.new_basic+'_' + obj.new_hra +'_'+ obj.gross+'_'+ obj.new_payment+'_' + obj.new_conv +
				'_' + obj.new_pf_one+'_' + obj.new_pt+'_' + obj.new_payabledays +'_' + obj.da +'_' + obj.medical_allowance + '_' + obj.personal_pay + '_' + obj.new_loan + 
				'_'  + obj.new_transport_allowance + '_'+obj.new_advance+'_' + obj.new_esic_one+'">' + counter+'</td>';
				salary_tr += '<td>' + obj.emp_name+'</td>';
				salary_tr += '<td>' + obj.new_payabledays +'</td>';
				salary_tr += '<td>' + obj.new_total_monthdays+'</td>';
				if(loginType != 'user'){
					salary_tr += '<td>' + obj.basic +'</td>';
					salary_tr += '<td>' + obj.da +'</td>';
					salary_tr += '<td>' + obj.hra +'</td>';
					salary_tr += '<td>' + obj.conv +'</td>';
				}
				salary_tr += '<td>' + obj.uan_no+'</td>';
				salary_tr += '<td>' + obj.esic_no+'</td>';
				salary_tr += '<td>' + obj.new_basic+'</td>';
				salary_tr += '<td>' + obj.new_da +'</td>';
				salary_tr += '<td>' + obj.new_hra +'</td>';
				salary_tr += '<td>' + obj.new_conv + '</td>';
				salary_tr += '<td>' + obj.medical_allowance + '</td>';
				salary_tr += '<td>' + obj.personal_pay + '</td>';
				salary_tr += '<td>' + obj.new_over_time + '</td>';	
				salary_tr += '<td>' + obj.new_incentives + '</td>';			
				salary_tr += '<td>' + obj.new_transport_allowance + '</td>';				
				salary_tr += '<td>' + obj.gross+'</td>';
				salary_tr += '<td>' + obj.new_esic_one+'</td>';
				salary_tr += '<td>' + obj.new_pt+'</td>';
				salary_tr += '<td>' + obj.new_pf_one+'</td>';
				salary_tr += '<td>' + obj.new_net+'</td>';
				salary_tr += '<td>' + obj.new_pf_two+'</td>';
				salary_tr += '<td>' + obj.new_pf_three+'</td>';
				salary_tr += '<td>' + obj.new_esic_two+'</td>';
				salary_tr += '<td>' + obj.new_payment+'</td>';
				salary_tr += '<td>' + obj.new_ctc+'</td>';

				var new_transport = isEmpty(obj.new_transport_allowance) ? 0 : obj.new_transport_allowance;
				var new_overtime = isEmpty(obj.new_over_time) ? 0 : obj.new_over_time;
				var old_overtime = isEmpty(obj.over_time) ? 0 : obj.over_time;
				var new_incentives = isEmpty(obj.new_incentives) ? 0 : obj.new_incentives;
				var new_loan = isEmpty(obj.new_loan) ? 0 : obj.new_loan;
				var new_advance = isEmpty(obj.new_advance) ? 0 : obj.new_advance;
				var medical = isEmpty(obj.medical_allowance) ? 0 : obj.medical_allowance;
				var pf8 = isEmpty(obj.pfdeduction_eight) ? 0 : obj.pfdeduction_eight;
				var pf12 = isEmpty(obj.pfdeduction_twelve) ? 0 : obj.pfdeduction_twelve;
				var revised_month = isEmpty(obj.revised_month) ? 0 : obj.revised_month;
				var salaryfixed_month = isEmpty(obj.salary_fixedmonth) ? 0 : obj.salary_fixedmonth;
				salary_tr += '<td><button class="btn btn-info btn-mini" type="button" id="' + obj.emp_name+'_'+establishment+'_'+location+'" onclick=editSalary(' + obj.new_payabledays +',this.id,' + 
				new_transport + ',' + new_overtime + ','+old_overtime+',' + new_incentives + ','+new_loan+','+new_advance+','+obj.salary_month+','+obj.str_id+','+obj.reg_id+','+obj.newstr_id+','+obj.basic+','
				+obj.da+','+obj.hra+','+obj.conv+',"'+obj.gender+'",'+medical+','+pf8+','+pf12+','+revised_month+','+salaryfixed_month+','+obj.esic+','+obj.esic_two+','+obj.pf_one+','+obj.pf_two+','+obj.pf_three+')>EDIT</button> <button class="btn btn-danger btn-mini" type="button" onclick=deleteSalary('+obj.str_id+','+obj.reg_id+','+obj.newstr_id+')>DELETE</button></td></tr>';
			});
			if(salary_tr != ''){
				var salary_table = '<table class="table table-bordered" id="salary_list"><thead><tr>';
				salary_table += '<th>Sr. No</th>';
				salary_table += '<th>Name</th>';
				salary_table += '<th>Payable Days</th>';
				salary_table += '<th>Month Format</th>';
				if(loginType != 'user'){
					salary_table += '<th id="old_basic_th">Old Basic</th>';
					salary_table += '<th id="old_da_th">Old DA</th>';
					salary_table += '<th id="old_hra_th">Old HRA</th>';
					salary_table += '<th id="old_conv_th">Old Conv.</th>';
				}
				salary_table += '<th>UAN No.</th>';
				salary_table += '<th>ESIC No.</th>';
				salary_table += '<th>Basic </th>';
				salary_table += '<th>DA</th>';
				salary_table += '<th>HRA</th>';
				salary_table += '<th>Conv</th>';
				salary_table += '<th>Medical</th>';
				salary_table += '<th>Personal Pay</th>';
				salary_table += '<th>Over Time</th>';
				salary_table += '<th>Incentives</th>';
				salary_table += '<th>Transport Allowance</th>';
				salary_table += '<th>Gross</th>';
				salary_table += '<th>ESIC <span>('+fixed_esic+')</span></th>';
				salary_table += '<th>PT</th>';
				salary_table += '<th>PF <span>('+fixed_pf12+')</span></th>';
				salary_table += '<th>Net Earning</th>';
				salary_table += '<th>PF <span>('+fixed_pf8+')</span></th>';
				salary_table += '<th>PF <span>('+fixed_pf3+')</span></th>';
				salary_table += '<th>ESIC <span>('+fixed_esic2+')</span></th>';
				salary_table += '<th>Payment</th>';
				salary_table += '<th>CTC</th><th>Action</th></tr></thead><tbody>';
				salary_table += salary_tr;
				salary_table += '</tbody></table>'
			}
			$("#savedList").html(salary_table);
			$("#salary_list").DataTable({
				dom: 'Bfrtip',
				buttons: [{
					extend: 'excelHtml5',
					exportOptions: {
						columns: ':visible'
					},
					filename: function(){                
						return establishment +"(" +location + ")";
					},
				},
				'colvis'
				]
			})
		},
		error: function(){
			toastr.error("Error in generating salary of " +emp_name,{timeout:5000});
		}
	});
}

function editSalary(payable_days,emp_est_loc,transport,new_overtime,old_overtime,incentives,loan,advance,salary_month,empid,regid,new_emp_id,old_basic,old_da,old_hra,old_conv,gender,medical,pf8_deduction,pf12_deduction,revised_month,fixed_month,
	fixed_esic,fixed_esic2,fixed_pf1,fixed_pf2,fixed_pf3){
	var emp_est_location = emp_est_loc.split("_");
	setEstablishment(emp_est_location[1]);
	setLocation(emp_est_location[2]);
	setEmployee(emp_est_location[0]);
	setSalaryMonth(salary_month);
	$("#payable_days").val(payable_days);
	$("#over_time_structural").val(old_overtime);
	$("#over_time").val(new_overtime);
	$("#incentive").val(incentives);
	$("#loan").val(loan);
	$("#advance").val(advance);
	$("#transport_allowances").val(transport);
	$("#update_salary").show();
	$("#edit_salary").hide();
	$("#structure_strid").val(empid);
	$("#structure_regid").val(regid);
	$("#new_empid").val(new_emp_id);
	$("#structure_basic").val(old_basic);
	$("#structure_da").val(old_da);
	$("#structure_conv").val(old_conv);
	$("#structure_hra").val(old_hra);
	$("#structure_gender").val(gender);
	$("#structure_medical").val(medical);
	$("#structure_pf8").val(pf8_deduction);
	$("#structure_pf12").val(pf12_deduction);
	$("#structure_revised_month").val(revised_month);
	$("#structure_salary_fixed").val(fixed_month);
	// $("#structure_personalpay").val(personal_pay);
	$("#fixed_esic").val(fixed_esic);
	$("#fixed_esic1").val(fixed_esic2);
	$("#fixed_pf12").val(fixed_pf1);
	$("#fixed_pf8").val(fixed_pf2);
	$("#fixed_pf3").val(fixed_pf3);
	toastr.info("Please scroll above to update the salary of selected employee","Successfully Edited Salary in above form",{timeout:5000});

}

function setEstablishment(est) {
	var objSelect = document.getElementById("establishment_name");
	setSelectedValue(objSelect,est);      
}
function setLocation(location) {
	var objSelect = document.getElementById("location");
	setSelectedValue(objSelect,location); 
}
function setEmployee(emp) {
	var objSelect = document.getElementById("employee_name");
	setSelectedValue(objSelect,emp); 
}
function setSalaryMonth(salary) {
	var objSelect = document.getElementById("salary_month");
	setSelectedValue(objSelect,salary);      
}
function setSelectedValue(selectObj, valueToSet) {
	for (var i = 0; i < selectObj.options.length; i++) {
		if (selectObj.options[i].text == valueToSet) {
			selectObj.options[i].selected = true;
			return;
		}
	}
}

function deleteSalary(strid,regid,newstrid){
	var salaryVo = new Object();
	salaryVo.str_id = strid;
	salaryVo.reg_id =regid;
	salaryVo.newstr_id = newstrid;

	var yes = confirm('Are You sure you want to delete this calculation?');
	if(yes){
		$.ajax({
			url:host+"/delete_newsalary.php",
			dataType:'json',
			type : 'POST',
			data : JSON.stringify(salaryVo),
			success : function(response){
				console.log(response);
				if(response.status == 'success')
					toastr.success('Successfully Deleted Calculation',{timeout:5000});
			},
			error:function(e){
				toastr.error('Error in Updating Calculation',{timeout:5000});
			}
		});

	}
	else{
		return false;
	}
}

function updateSalary(){
	calculationSalary();
	var empVO = new Object();
	empVO.salary_month = month_selected;
	empVO.overtime = over_time;
	empVO.payable_days = payable_days;
	empVO.total_month_days = total_month_days;
	empVO.new_basic = new_basic;
	empVO.new_da = new_da;
	empVO.new_hra = new_hra;
	empVO.new_conv = new_conv;
	empVO.overtime = over_time;
	empVO.transport_allowances = transport_allowances;
	empVO.gross = gross;
	empVO.esic = esic;
	empVO.pt = pt;
	empVO.pf = pf;
	empVO.net = net;
	empVO.pf_2 = pf_2;
	empVO.pf_3 = pf_3;
	empVO.esic_2 = esic_2;
	empVO.payment = payment;
	empVO.ctc = ctc;
	empVO.loan = loan;
	empVO.advance = advance;
	empVO.incentives = incentives;
	empVO.str_id = $("#structure_strid").val();
	empVO.reg_id = $("#structure_regid").val();
	empVO.newstr_id = $("#new_empid").val();
	$.ajax({
		url:host+"/edit_newsalary.php",
		dataType:'json',
		type : 'POST',
		data : JSON.stringify(empVO),
		success : function(response){
			console.log("Updated",response);
			// if(response.status == 'success'){
				toastr.success('Successfully updated Calculation',{timeout:5000});
				$("#update_salary").hide();
				$("#edit_salary").show();
				getCalculteSalaryData();
// }	
},
error:function(e){
	toastr.error('Error in Updating Calculation',{timeout:5000});
}
});
}


function salarySlip(){
	getCalculteSalaryData();
	$("#salary-slip-li").addClass('active');
	$("#salary-calculation-li").removeClass('active');
	$("#pf-challan-li").removeClass('active');
	$("#esic-challan-li").removeClass('active');
	var currentmonth = new Date().getFullYear();
	var month_selected = $("#salary_month option:selected").text();
	if(month_selected == "--Select --")
		alert("Please select Salary month");
	if($("#employee_name option:selected").val() == 0){
		alert("Please Select Employee Name");
		return false;
	}
	var emp_id = $("#structure_strid").val();
	var generated_data = $("#employeeData-"+emp_id).val().split("_");
	var emp_name = $("#employee_name option:selected").text();
	var company_name = $("#establishment_name").val();
	var esic_no = $("#structure_esic_no").val();
	var uan_no = $("#structure_uan").val();
	var old_basic = $("#structure_basic").val();

	$("#emp-basic").text(generated_data[0]);
	$("#emp-hra").text(generated_data[1]);

	var transport = $("#transport_allowances").val();
	var personal_pay = $("#structure_personalpay").val();

	/*var loan =$("#loan").val();
	var advance = $("#advance").val();*/


	var salary_div = '<br><div class="table-responsive salary-slip-table-result col-sm-12" id="salary-slip-list">'; 
	salary_div+='<h3 style="text-align: center;">PAYSLIP FOR THE MONTH OF <span id="salary-slip-month-year">'+month_selected + ' '+ currentmonth+'</span></h3>';

	salary_div+='<h5 class="company_name col-xs-6 form-group p-0">COMPANY :- <span id="comapny-name">'+company_name+'</span></h5>';
	salary_div+='<h5 class="col-xs-6 form-group p-0 employee_name">Employee NAME :  <span id="employee-name">'+emp_name+'</span></h5>';

	salary_div+='<table class="table table-striped">';
	salary_div+='<tr><th>BANK /CASH</th><th>PF.NO.</th><th>ESIC NO.</th><th>BASIC Rt.</th><th>ATTND.</th><th></th><th></th></tr>';
	salary_div+='<tr><td>BANK</td><td id="emp-pf-no">'+uan_no+'</td><td id="emp-esic-no">'+esic_no+'</td><td id="emp-basic-rate">'+old_basic+'</td><td id="emp-attendance">'+generated_data[7]+'</td><td></td><td></td></tr>';

	salary_div+='<tr><th>BASIC</th><th>DA</th><th>HRA</th><th>CONV</th><th>MED Al.</th><th>PP</th><th>Transport Al.</th></tr>';
	salary_div+='<tr><td id="emp-basic">'+generated_data[0]+'</td><td>'+generated_data[8]+'</td><td id="emp-hra">'+generated_data[1]+'</td><td id="emp-conv">'+generated_data[4]+'</td><td id="emp-medical">'+generated_data[9]+'</td><td id="emp-pp">'+generated_data[10]+'</td><td id="emp-transport">'+generated_data[12]+'</td></tr>';

	salary_div+='<tr><th>PF</th><th>ESIC</th><th>P.TAX</th><th>LOAN</th><th>ADVANCE</th><th>LWF</th><th>Other DDN</th></tr>';
	salary_div+='<tr><td id="emp-pf">'+generated_data[5]+'</td><td id="emp-esic">'+generated_data[14]+'</td><td id="emp-pt">'+generated_data[6]+'</td><td id="emp-loan">'+generated_data[11]+'</td><td id="emp-advance">'+generated_data[13]+'</td><td id="emp-lwf"></td><td id="emp-other-deduction"></td></tr>';

	salary_div+='<tr><th>TTL EARNING</th><th>PS.BF</th><th>GROSS EARNING</th><th>TTL DED.</th><th>PS.CF</th><th>NET PAYABLE</th><th></th></tr>';
	salary_div+='<tr><td id="emp-total-salary"></td><td id="emp-ps"></td><td id="emp-gross">'+generated_data[2]+'</td><td id="emp-total-deduction"></td><td id="emp-ps-cf"></td><td id="emp-net-earning">'+generated_data[3]+'</td><td></td></tr>';
	salary_div+='</table></div>';

	$("#salary_table_list").append(salary_div);
}

function exportHTML(){
	var year = new Date().getFullYear();
	var salarymonth = $("#salary_month option:selected").text();
	var company = $("#establishment_name").val();
  /*  var header = "<html xmlns:o='urn:schemas-microsoft-com:office:office' "+
            "xmlns:w='urn:schemas-microsoft-com:office:word' "+
            "xmlns='http://www.w3.org/TR/REC-html40'>"+
            "<head><meta charset='utf-8'><title>Export HTML to Word Document with JavaScript</title></head><body>";
    var sourceHTML = header+document.getElementById("salary-slip-list").innerHTML+'</body></html>';
    var css = (
     '<style>' +
     '@page salary-slip-table-result{size: 841.95pt 595.35pt;mso-page-orientation: landscape;}' +
     'div.salary-slip-table-result {page: salary-slip-table-result;}' +
     'table{border-collapse:collapse;}td{border:1px gray solid;width:5em;padding:2px;}'+
     '</style>'
   );
       var blob = new Blob(['\ufeff', css + sourceHTML], {
        type: 'application/msword'
    });
    var source = 'data:application/vnd.ms-word;charset=utf-8,' + encodeURIComponent(sourceHTML);
     filename = company?company +'_'+salarymonth+'_'+year+'.doc':'document.doc';
    var downloadLink = document.createElement("a");
   
if(navigator.msSaveOrOpenBlob ){
        navigator.msSaveOrOpenBlob(blob, filename);
    }else{
      
        downloadLink.href = source;
        
        
        downloadLink.download = filename;
        
        downloadLink.click();
    }
    
    document.body.removeChild(downloadLink);*/





    var html, link, blob, url, css;

    css = (
    	'<style>' +
    	'@page WordSection1{size: 1000.95pt 595.35pt;mso-page-orientation: landscape;}' +
    	'div.WordSection1 {page: WordSection1;width:100%}' +
    	'table{border-collapse:collapse;width:100%}th{border:1px gray solid;width:5em;padding:2px;}td{border:1px gray solid;width:5em;padding:2px;}'+
    	'</style>'
    	);

    html = document.getElementById("salary_table_list").innerHTML;
    blob = new Blob(['\ufeff', css + html], {
    	type: 'application/msword'
    });
    url = URL.createObjectURL(blob);
    link = document.createElement('A');
    link.href = url;
   // Set default file name. 
   // Word will append file extension - do not add an extension here.
   link.download = company?company +'_'+salarymonth+'_'+year+'.doc':'document.doc';   
   document.body.appendChild(link);
   if (navigator.msSaveOrOpenBlob ) navigator.msSaveOrOpenBlob( blob,  company?company +'_'+salarymonth+'_'+year+'.doc':'document.doc'); // IE10-11
   		else link.click();  // other browsers
   		document.body.removeChild(link);

   	}
   	function pfChallan(){
   		var counter = 0;
   		var salarymonth = $("#salary_month").val();
   		var establishment = $("#establishment_name").val();
   		var location = $("#location").val();
   		var salaryVO = new Object();
   		salaryVO.establishment_name = establishment;
   		salaryVO.est_location = location;
   		salaryVO.salary_month = salarymonth;

   		$.ajax({
   			url:host+'/alldatafetch.php',
   			dataType:'json',
   			type : 'POST',
   			data : JSON.stringify(salaryVO),
   			success : function(response){
   				var pf_challan_tr='';
   				$.each(response,function(i,obj){
   					var epf_wages = obj.new_basic + obj.da;
   					var eps_wages;				
   					pf_challan_tr +='<tr>';
   					pf_challan_tr +='<td>' + obj.uan_no +'</td>';
   					pf_challan_tr +='<td>' + obj.emp_name +'</td>';
   					pf_challan_tr +='<td>' + obj.gross +'</td>';
   					pf_challan_tr +='<td>' + epf_wages + '</td>';
   					if(epf_wages > 15000){	
   						eps_wages = 15000;
   						pf_challan_tr +='<td>15000</td>';
   						pf_challan_tr +='<td>15000</td>';
   					}
   					else{
   						eps_wages = epf_wages;
   						pf_challan_tr +='<td>'+epf_wages+'</td>';
   						pf_challan_tr +='<td>'+epf_wages+'</td>';
   					}
   					pf_challan_tr +='<td>' + obj.new_pf_one+'</td>';

   					var eps_contri = Math.round((eps_wages * obj.pf_two)/100);

   					pf_challan_tr +='<td>' + eps_contri +'</td>';

   					var epf_eps_diff = obj.new_pf_one - eps_contri;

   					pf_challan_tr +='<td>' + epf_eps_diff +'</td>';
   					pf_challan_tr +='<td>0</td>';
   					pf_challan_tr +='<td>0</td><tr>';

   				});
   				if(pf_challan_tr!=''){
   					var pf_challan_table = '<table class="table table-bordered" id="pf-challan-datatable"><thead><tr><th>UAN No</th><th>Employee Name</th>';
   					pf_challan_table +='<th>Gross Wages</th><th>EPF Wages</th><th>EPS Wages</th><th>EDLI Wages</th>';
   					pf_challan_table +='<th>EPF Contri Remitted</th><th>EPS Contri Remitted</th><th>EPF EPS Diff Remitted</th>';
   					pf_challan_table +='<th>NCP Days</th><th>Refund of Advances</th></tr></thead><tbody>';
   					pf_challan_table += pf_challan_tr + '</tbody></table>';
   				}
   				$("#pf-challan-list").html(pf_challan_table);
   				$("#pf-challan-datatable").DataTable({
   					dom: 'Bfrtip',
   					buttons: [{
   						extend: 'excelHtml5',
   						exportOptions: {
   							columns: ':visible'
   						},
   						filename: function(){                
   							return establishment +"(" +location + ")";
   						},
   					},
   					'colvis'
   					]
   				});
   			},
   			error:function(e){
   				toastr.error("Error in fetching challan details. ",{timeout:5000})
   			}
   		});
   	}

   	function esicChallan(){
   		var counter = 0;
   		var salarymonth = $("#salary_month").val();
   		var establishment = $("#establishment_name").val();
   		var location = $("#location").val();
   		var salaryVO = new Object();
   		salaryVO.establishment_name = establishment;
   		salaryVO.est_location = location;
   		salaryVO.salary_month = salarymonth;

   		$.ajax({
   			url:host+'/alldatafetch.php',
   			dataType:'json',
   			type : 'POST',
   			data : JSON.stringify(salaryVO),
   			success : function(response){
   				var esic_challan_tr='';
   				$.each(response,function(i,obj){
   					esic_challan_tr +='<tr>';
   					esic_challan_tr +='<td>' + obj.esic_no +'</td>';
   					esic_challan_tr +='<td>' + obj.emp_name +'</td>';
   					esic_challan_tr +='<td>' + obj.new_payabledays +'</td>';
   					esic_challan_tr +='<td>' + obj.gross +'</td></tr>';
   				});
   				if(esic_challan_tr!=''){
   					var esic_challan_table = '<table class="table table-bordered" id="esic-challan-datatable"><thead><tr><th>ESIC No</th><th>Employee Name</th><th>Payable Days</th><th>Total Month Wages</th></tr></thead><tbody>';
   					esic_challan_table += esic_challan_tr + '</tbody></table>';
   				}
   				$("#esic-challan-list").html(esic_challan_table);
   				$("#esic-challan-datatable").DataTable({
   					dom: 'Bfrtip',
   					buttons: [{
   						extend: 'excelHtml5',
   						exportOptions: {
   							columns: ':visible'
   						},
   						filename: function(){                
   							return establishment +"(" +location + ")";
   						},
   					},
   					'colvis'
   					]
   				});
   			},
   			error:function(e){
   				toastr.error("Error in fetching challan details. ",{timeout:5000})
   			}
   		});
   	}

