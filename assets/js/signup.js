function signup(){
var signupVO = new Object();
signupVO.company_name = $("#company-name").val();
signupVO.company_address = $("#company-address").val();
signupVO.email_id = $("#email-id").val();
signupVO.mobile_no = $("#mobile-no").val();
signupVO.role = $("#role").val();
signupVO.company_esic_no = $("#company-esic-no").val();
signupVO.company_pf_no = $("#company-pf-no").val();
signupVO.username = $("#username").val();
signupVO.password = $("#password").val();

$.ajax({
		url:host+'/signup.php',
		dataType:'json',
		type : 'POST',
		data : JSON.stringify(signupVO),
		success : function(response){
			console.log(response);
			if(response.status == 'success'){
				window.location.href='index.php';
			}
		},
		error:function(e){
			toastr.error("Error in fetching challan details. ",{timeout:5000});
		}
	});

}
