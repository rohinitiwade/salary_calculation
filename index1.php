<?php include("config.php");?><!DOCTYPE html>
<html lang="en">

<!-- Mirrored from html.codedthemes.com/mash-able/light/landingpage/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 12 Jun 2019 18:22:28 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<head>
  <meta charset="utf-8">
  <title>Landing Page - Mash able Bootstrap4 admin template</title>
  <!-- Meta -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="Landing page template for creative dashboard">
  <meta name="keywords" content="Landing page template">
  <!-- Favicon icon -->
  <link rel="icon" href="assets/logos/favicon.ico" type="image/png" sizes="16x16">
  <!-- Bootstrap -->
  <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" media="all" />
  <!-- Font -->
  <link href="assets/css/opensans.css" rel="stylesheet">
  <link href="assets/css/poppins.css" rel="stylesheet" type="text/css">
  <!-- Animate CSS -->
  <link rel="stylesheet" href="assets/css/animate.css">
  <!-- Owl Carousel -->
  <link rel="stylesheet" href="assets/css/owl.carousel.css">
  <link rel="stylesheet" href="assets/css/owl.theme.css">
  <!-- Magnific Popup -->
  <link rel="stylesheet" href="assets/css/magnific-popup.css">
  <!-- Full Page Animation -->
  <link rel="stylesheet" href="assets/css/animsition.min.css">
  <!-- Ionic Icons -->
  <link rel="stylesheet" href="assets/css/ionicons.min.css"> 
  <!-- Main Style css -->
  <link href="assets/css/style.css" rel="stylesheet" type="text/css" media="all" />
  <link rel="stylesheet" type="text/css" href="assets/css/icofont.css">
  <link rel="stylesheet" href="assets/css/main_page.min.css">
  
  <style type="text/css">
    .dataTables_info{
      float: left;
      display: inline-block;
    }
    #structural_table_filter{
      float: right;
    }

    .checkbox-fade label {
      line-height: 25px;
    }
    .structural-content{
      padding-top: 3%;
    }
    .checkbox-fade{
      margin-right: 0px;
    }
    .checkbox-fade.fade-in-primary .cr {
      border: 2px solid #0073aa;
    }
    .checkbox-fade .cr {
      border-radius: 0;
      border: 2px solid #0073aa;
      cursor: pointer;
      display: inline-block;
      float: left;
      height: 20px;
      margin-right: .5em;
      position: relative;
      width: 20px;
    }
    .checkbox-fade label {
      line-height: 25px;
    }
    .checkbox-fade label:after {
      content: '';
      display: table;
      clear: both;
    }
    .nav>li>a {
      position: relative;
      display: block;
      padding: 0px 15px;
    }
    .navbar-nav>li {
      float: right;
    }
    .past-main .nav>li>a {
      position: relative;
      display: block;
      padding: 10px 15px;
    }
    .past-main .login_button{
      margin-top:1%;
    }
  </style>
</head>
<body>

  <div class="wrapper animsition" data-animsition-in-class="fade-in"
  data-animsition-in-duration="1000"
  data-animsition-out-class="fade-out"
  data-animsition-out-duration="1000">
  <div class="container">
    <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
      <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header page-scroll">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand page-scroll" href="#main">Salary Calculation</a>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse navbar-right" id="bs-example-navbar-collapse-1">
          <ul class="nav navbar-nav">
            <li class="col-sm-6 row" style="float: right;">
              <form><li class="col-sm-5 p-r-0"><input type="text"
                name="userCode" placeholder="UserName/Email"
                class="form-control m-0" id="user-name">
              </li>
              <li class="col-sm-5 p-r-0">
                <input type="password" class="form-control m-0" id="password" placeholder="Password"
                name="password" style="position: relative;"> 
                <span class="field-icon ion-android-warning" title="Caps Lock is ON"></span>
              </li>
              <li class="login_button col-sm-2"><button
                class="btn btn-primary btn-mini" type="button" onclick="login()">Login</button>
              </li>
            </form>
            </li>
            <li><a class="page-scroll" href="calculate_salary.php">Contact Us</a></li>
            <li><a class="page-scroll" href="#form">Services</a></li>
            <li><a class="page-scroll" href="#form">About Us</a></li> <li><a class="page-scroll" href="#fixed_values">Home</a></li>

          </ul>
        </div>
      </div>
    </nav><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->

  <div class="main" id="main"><!-- Main Section-->
    <div class="hero-section app-hero">
      <div class="container">
        <div class="hero-content app-hero-content text-center">
          <div class="col-md-10 col-md-offset-1 nopadding">
            <h1 class="wow fadeInUp" data-wow-delay="0s">Employee Salary Calculation</h1>
            <div class="wow fadeInUp" data-wow-delay="0.2s">
              <a href="demo-page.html"><button class="btn btn-primary" type="button">Free Demo</button></a>
            </div>

          </div>
          <div class="col-md-12">
            <div class="hero-image">
              <img class="img-responsive" src="assets/images/app_hero_1.png" alt="" />
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="services-section text-center" id="fixed_values">
      <div class="container">
        <div class="col-md-8 col-md-offset-2 nopadding">
          <div class="services-content">
            <h1 class="wow fadeInUp" data-wow-delay="0s">About Us</h1>
            <p>This form is to be used for entering fixed values of ESIC, PF.</p>
          </div>
          <div>
          </div>

        </div>

      </div>
    </div>
    <div class="flex-features text-center" id="form"><!-- Services section (small) with icons -->
      <div class="container">
        <div class="col-md-8 col-md-offset-2 nopadding">
          <div class="services-content">
            <h1 class="wow fadeInUp" data-wow-delay="0s" style="visibility: visible; animation-delay: 0s; animation-name: fadeInUp;">We take care our products for more feature rich</h1>
            <p class="wow fadeInUp" data-wow-delay="0.2s" style="visibility: visible; animation-delay: 0.2s; animation-name: fadeInUp;">
              Mash Able is one of the finest Admin dashboard template in its category. Premium admin dashboard with high end feature rich possibilities.
            </p>
          </div>
        </div>
        <div class="col-md-12 text-center">
          <div class="services">
            <div class="col-sm-4 wow fadeInUp" data-wow-delay="0.2s" style="visibility: visible; animation-delay: 0.2s; animation-name: fadeInUp;">
              <div class="services-icon">
                <img src="assets/images/service1.png" height="60" width="60" alt="Service">
              </div>
              <div class="services-description">
                <h1>Mega feature rich</h1>
                <p>
                  Mash Able is one of unique dashboard template which come with tons of ready to use feature. We continuous working on it to provide latest updates in digital market.
                </p>
              </div>
            </div>
            <div class="col-sm-4 wow fadeInUp" data-wow-delay="0.3s" style="visibility: visible; animation-delay: 0.3s; animation-name: fadeInUp;">
              <div class="services-icon">
                <img class="icon-2" src="assets/images/service2.png" height="60" width="60" alt="Service">
              </div>
              <div class="services-description">
                <h1>Fast and Robust</h1>
                <p>
                  We are contantly working on Mash Able and improve its performance too. Your definitely give higher rating to Mash Able for its performance.
                </p>
              </div>
            </div>
            <div class="col-sm-4 wow fadeInUp" data-wow-delay="0.4s" style="visibility: visible; animation-delay: 0.4s; animation-name: fadeInUp;">
              <div class="services-icon">
                <img class="icon-3" src="assets/images/service3.png" height="60" width="60" alt="Service">
              </div>
              <div class="services-description">
                <h1>FLAT UI-Interface</h1>
                <p>
                  Mash able is first ever admin dashboard template which release in Bootstrap 4 framework. Intuitive feature rich design concept and color combination.
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="services-section text-center" id="contact"> 
      <div class="container structural-content">
        <div class="col-md-8 col-md-offset-2 nopadding">
          <div class="services-content">
            <h1 class="wow fadeInUp" data-wow-delay="0s" style="visibility: visible; animation-delay: 0s; animation-name: fadeInUp;">Contact Us</h1>

          </div>
        </div>
        <div class="col-sm-12" id="map">
          <iframe width='100%' height='500px' id='mapcanvas' src='https://maps.google.com/maps?q=Ujjwal%20Nagar%20Kawta%20road,%20naka%20no.%202&amp;t=&amp;z=10&amp;ie=UTF8&amp;iwloc=&amp;output=embed' frameborder='0' scrolling='no' marginheight='0' marginwidth='0'><div class="zxos8_gm"><a href="https://www.embedgooglemap.co.uk/galaxy-a70-deals.html">here</a></div><div style='overflow:hidden;'><div id='gmap_canvas' style='height:100%;width:100%;'></div></div><div><small>Powered by <a href="https://www.embedgooglemap.co.uk">Embed Google Map</a></small></div></iframe>
        </div>
      </div>   
    </div>

    <!-- Scroll To Top -->

    <a id="back-top" class="back-to-top page-scroll" href="#main">
      <i class="ion-ios-arrow-thin-up"></i>
    </a>

    <!-- Scroll To Top Ends-->


  </div><!-- Main Section -->
</div><!-- Wrapper-->

<!-- Jquery and Js Plugins -->
<script type="text/javascript" src="assets/js/jquery-2.1.1.js"></script>
<script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
<script type="text/javascript" src="assets/js/plugins.js"></script>
<script type="text/javascript" src="assets/js/menu.js"></script>
<script type="text/javascript" src="assets/js/custom.js"></script>
<script type="text/javascript" src="assets/js/connectionutils.js"></script>
<script type="text/javascript" src="assets/js/login.js"></script>

<script src="assets/js/data-table/jquery.dataTables.min.js"></script>
<script src="assets/js/data-table/dataTables.bootstrap4.min.js"></script>
<script type="text/javascript">
  $(".field-icon").hide();
  var input = document.getElementById("password");
  var caps = 'on';
  input.addEventListener("keyup", function(event) {
    if (event.key == "CapsLock") {
      if (caps == 'on') {
        $(".field-icon").show();
        caps = 'off';
      } else {
        $(".field-icon").hide();
        caps = 'on';
      }
    }
  });
</script>

</body>

<!-- Mirrored from html.codedthemes.com/mash-able/light/landingpage/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 12 Jun 2019 18:23:20 GMT -->
</html>