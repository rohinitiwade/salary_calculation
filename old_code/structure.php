<?php include("config.php");?><!DOCTYPE html>
<html lang="en">

<!-- Mirrored from html.codedthemes.com/mash-able/light/landingpage/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 12 Jun 2019 18:22:28 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<head>
  <meta charset="utf-8">
  <title>Landing Page - Mash able Bootstrap4 admin template</title>
  <!-- Meta -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="Landing page template for creative dashboard">
  <meta name="keywords" content="Landing page template">
  <!-- Favicon icon -->
  <link rel="icon" href="assets/logos/favicon.ico" type="image/png" sizes="16x16">
  <!-- Bootstrap -->
  <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" media="all" />
  <!-- Font -->
  <link href="assets/css/opensans.css" rel="stylesheet">
  <link href="assets/css/poppins.css" rel="stylesheet" type="text/css">
  <!-- Animate CSS -->
  <link rel="stylesheet" href="assets/css/animate.css">
  <!-- Owl Carousel -->
  <link rel="stylesheet" href="assets/css/owl.carousel.css">
  <link rel="stylesheet" href="assets/css/owl.theme.css">
  <!-- Magnific Popup -->
  <link rel="stylesheet" href="assets/css/magnific-popup.css">
  <!-- Full Page Animation -->
  <link rel="stylesheet" href="assets/css/animsition.min.css">
  <!-- Ionic Icons -->
  <link rel="stylesheet" href="assets/css/ionicons.min.css"> 
  <!-- Main Style css -->
  <link href="assets/css/style.css" rel="stylesheet" type="text/css" media="all" />
  <link rel="stylesheet" type="text/css" href="assets/css/icofont.css">
  <link rel="stylesheet" href="assets/css/main_page.min.css">
  <link rel="stylesheet" type="text/css" href="assets/css/dataTables.bootstrap4.min.css">
  <style type="text/css">
    .dataTables_info{
      float: left;
      display: inline-block;
    }
    #structural_table_filter{
      float: right;
    }
   
    .checkbox-fade label {
      line-height: 25px;
    }
    .structural-content{
      padding-top: 3%;
    }
    .checkbox-fade{
      margin-right: 0px;
    }
    .checkbox-fade.fade-in-primary .cr {
      border: 2px solid #0073aa;
    }
    .checkbox-fade .cr {
      border-radius: 0;
      border: 2px solid #0073aa;
      cursor: pointer;
      display: inline-block;
      float: left;
      height: 20px;
      margin-right: .5em;
      position: relative;
      width: 20px;
    }
    .checkbox-fade label {
      line-height: 25px;
    }
    .checkbox-fade label:after {
      content: '';
      display: table;
      clear: both;
    }
  </style>
</head>
<body>

  <div class="wrapper animsition" data-animsition-in-class="fade-in"
  data-animsition-in-duration="1000"
  data-animsition-out-class="fade-out"
  data-animsition-out-duration="1000">
  <div class="container">
    <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
      <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header page-scroll">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand page-scroll" href="#main">Salary Calculation</a>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse navbar-right" id="bs-example-navbar-collapse-1">
          <ul class="nav navbar-nav">

            <li><a class="page-scroll" href="#fixed_values">Fixed Values</a></li>
            <li><a class="page-scroll" href="#form">Structural Values</a></li>
            <li><a class="page-scroll" href="calculate_salary.php">Salary Calculation</a></li>
<li><a class="page-scroll" href="index.php">Logout</a></li>
          </ul>
        </div>
      </div>
    </nav><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->

  <div class="main" id="main"><!-- Main Section-->
    <div class="hero-section app-hero">
      <div class="container">
        <div class="hero-content app-hero-content text-center">
          <div class="col-md-10 col-md-offset-1 nopadding">
            <h1 class="wow fadeInUp" data-wow-delay="0s">Employee Salary Calculation</h1>
            <p class="wow fadeInUp" data-wow-delay="0.2s">
              First ever Bootstrap 4 admin template with Flat UI Interface. <br class="hidden-xs"> Its best choice for your any complex project.
            </p>

          </div>
          <div class="col-md-12">
            <div class="hero-image">
              <img class="img-responsive" src="assets/images/app_hero_1.png" alt="" />
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="services-section text-center" id="fixed_values">
      <div class="container">
        <div class="col-md-8 col-md-offset-2 nopadding">
          <div class="services-content">
            <h1 class="wow fadeInUp" data-wow-delay="0s">Please Provide following.</h1>
            <p>This form is to be used for entering fixed values of ESIC, PF.</p>
          </div>
          <div>
          </div>

        </div>
        <form method="POST" id="pf_table" class="table-responsive col-sm-12 p-0">
          <table class="table table-bordered" id="fixed_values_table">
            <thead>
              <tr>
                <th>ESIC</th>
                <th>ESIC</th>
                <th>PF (12%)</th>
                <th>PF (8.33%)</th>
                <th>PF (3.67%)</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              <tr>
               <!--  <td><input type="text" class="form-control" placeholder="Enter ESIC" id="fixed-esic" name="esic_no">
                </td>
                 <td><input type="text" class="form-control" placeholder="Enter ESIC" id="fixed-esic" name="esic_no">
                </td>
                <td><input type="text" class="form-control" placeholder="Enter PF (12%)" id="fixed-pf-12" name="pf1"></td>
                <td><input type="text" class="form-control" placeholder="Enter PF (8.33%)" id="fixed-pf-8" name="pf2"></td>
                <td><input type="text" class="form-control" placeholder="Enter PF (3.67%)" id="fixed-pf-3" name="pf3"></td>
                <td><button class="btn btn-success btn-mini" type="button" onclick="save_updateFixedValues('Save')">Save</button>
                  <button class="btn btn-success btn-mini" type="button" onclick="save_updateFixedValues('Edit')" id="updateFixed_val">Update</button></td> -->
                </tr>
              </tbody>
            </table>
          </div>
        </div>
        <div class="flex-features text-center" id="form"><!-- Services section (small) with icons -->
          <div class="container">
            <div class="col-md-8 col-md-offset-2 nopadding">
              <div class="services-content">
                <h1 class="wow fadeInUp" data-wow-delay="0s">Please Provide following Structural values.</h1>
                <p>This form is to be used for entering employee's basic, da,hra,conv etc. for salary calculation.This data to be entered once for every user,till the values are changed</p>
              </div>
            </div>
            <form method="POST" id="structual_id">
<!-- <div class="col-sm-3 wow fadeInUp" data-wow-delay="0s">
            <label>Establishment (Company) Name</label>
            <input type="text" id="basic" name="" class="form-control">
          </div> -->
          <input type="hidden" id="emp_id" name="">
          <div class="col-sm-12">
           <div class="col-sm-3 wow fadeInUp col-sm-offset-3" data-wow-delay="0.2s">
            <label style="color: red;font-weight: bold;">Establishment (Company) Name <span style="color: red">*</span></label>
            <textarea class="form-control" id="establishment_name" rows="1" ></textarea>
          </div>  
          <div class="col-sm-3 wow fadeInUp" data-wow-delay="0.2s">
            <label style="color: red;font-weight: bold;">Establishment Location </label>
            <input type="text" class="form-control" id="est_location" name="">
          </div> 
        </div>
        <div class="col-sm-12">

          <div class="col-sm-3 wow fadeInUp" data-wow-delay="0.2s">
            <label>UAN No. </label>
            <input type="text" id="uan_no" name="" maxlength="12" minlength="12" class="form-control" onkeypress="return isNumber(event)">
          </div>
          <div class="col-sm-3 wow fadeInUp" data-wow-delay="0.2s">
            <label>ESIC No. </label>
            <input type="text" id="esic_no" name="" class="form-control" onkeypress="return isNumber(event)">
          </div>
          <div class="col-sm-3 wow fadeInUp" data-wow-delay="0.2s">
            <label>Employee Name <span style="color: red">*</span></label>
            <input type="text" id="emp_name" name="" class="form-control">
          </div>
            <div class="col-sm-3 wow fadeInUp" data-wow-delay="0.2s">
            <label>Account Number </label>
            <input type="text" class="form-control" id="account_number" name="">
          </div> 
        </div>

        <div class="col-sm-12">
          <div class="col-sm-3 wow fadeInUp" data-wow-delay="0.2s">
            <label>Gender <span style="color: red">*</span></label>
            <select class="form-control" id="gender">
              <option value="">-- Select --</option>
              <option value="Male">Male</option>
              <option value="Female">Female</option>
            </select>
          </div> 
          <div class="col-sm-3 wow fadeInUp" data-wow-delay="0.2s">
            <label>Basic <span style="color: red">*</span></label>
            <input type="text" id="basic" name="" class="form-control" onkeypress="return isNumber(event)">
          </div>
          <div class="col-sm-3 wow fadeInUp" data-wow-delay="0.2s">
            <label>DA <span style="color: red">*</span></label>
            <input type="text" id="da" name="" class="form-control" onkeyup="getHRA()" onkeypress="return isNumber(event)">
          </div>
          <div class="col-sm-3 wow fadeInUp" data-wow-delay="0.2s">
            <label>HRA <span style="color: red">*</span></label>
            <input type="text" id="hra" name="" class="form-control" onkeypress="return isNumber(event)">
          </div>
          
        </div>

        <div class="col-sm-12">
          <div class="col-sm-3 wow fadeInUp" data-wow-delay="0.2s">
            <label>Conv. <span style="color: red">*</span></label>
            <input type="text" id="conv" name="" class="form-control" onkeypress="return isNumber(event)">
          </div>
          <div class="col-sm-3  wow fadeInUp" data-wow-delay="0.2s">
            <label>Medical Allowance</label>
            <input type="text" id="medical_allowance" name="" class="form-control" onkeypress="return isNumber(event)">
          </div>
          <!-- <div class="col-sm-3 wow fadeInUp" data-wow-delay="1.2s">
            <label>Incentives</label>
            <input type="text" id="incentives" name="" class="form-control">
          </div> -->
          <div class="col-sm-3 wow fadeInUp" data-wow-delay="0.2s">
            <label>Over Time Wages Per Hour / Per Day</label>
            <input type="text" id="over_time" name="" class="form-control" onkeypress="return isNumber(event)">
          </div>
          <div class="col-sm-3 wow fadeInUp" data-wow-delay="0.2s">
            <label>Personal Pay</label>
            <input type="text" id="personal_pay" name="" class="form-control">
          </div>
         

        </div>
        <div class="col-sm-12 form-group">
          <div class="col-sm-3 checkbox-fade fade-in-primary wow fadeInUp" data-wow-delay="0.2s">
            <label>
              <input type="checkbox" value="" name="pf_salary_12">
              <span class="cr">
                <i class="cr-icon icofont icofont-ui-check txt-primary"></i>
              </span> <span> Pf(12%) deduction on 15K</span>
            </label>
            <!-- <label><input type="checkbox" name="pf_salary"></label> -->
          </div>
          <div class="col-sm-3 checkbox-fade fade-in-primary wow fadeInUp" data-wow-delay="0.2s">
            <label>
              <input type="checkbox" value="" name="pf_salary_8">
              <span class="cr">
                <i class="cr-icon icofont icofont-ui-check txt-primary"></i>
              </span> <span> Pf(8.33%) deduction on 15K</span>
            </label>
            <!-- <label><input type="checkbox" name="pf_salary"></label> -->
          </div>
          <div class="col-sm-3 checkbox-fade fade-in-info wow fadeInUp" data-wow-delay="0.2s">
           <label>
            <input type="checkbox" value="" name="salary_month_days" id="salary_check">
            <span class="cr">
              <i class="cr-icon icofont icofont-ui-check txt-info"></i>
            </span> <span> Salary on Fixed Month Days</span>
          </label>
          <!-- <label><input type="checkbox" name="salary_month_days" id="salary_check"> Salary on Fixed Month Days</label> -->
        </div>
        <div class="col-sm-3" id="revised_month_div">
          <label>Revised Month Days</label>
          <input class="form-control" id="revised_month_days" name="" onkeypress="return isNumber(event)">
        </div>
        
      </div>    
      <div class="col-sm-12 text-center">
        <button type="button" class="btn btn-info btn-sm" onclick="submitValue()" id="submit_structure">Submit</button>
        <button  type="button" class="btn btn-info btn-sm" onclick="updateEntry()" id="update_structure">Update</button>
      </div>

    </form>
  </div>
</div>

<div class="services-section text-center"> 
  <div class="container structural-content">
    <div class=" table-responsive col-sm-12 " id="savedList">
    </div>
  </div>   
</div>

<!-- Scroll To Top -->

<a id="back-top" class="back-to-top page-scroll" href="#main">
  <i class="ion-ios-arrow-thin-up"></i>
</a>

<!-- Scroll To Top Ends-->


</div><!-- Main Section -->
</div><!-- Wrapper-->

<!-- Jquery and Js Plugins -->
<script type="text/javascript" src="assets/js/jquery-2.1.1.js"></script>
<script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
<script type="text/javascript" src="assets/js/plugins.js"></script>
<script type="text/javascript" src="assets/js/menu.js"></script>
<script type="text/javascript" src="assets/js/custom.js"></script>
<script type="text/javascript" src="assets/js/connectionutils.js"></script>
<script type="text/javascript" src="assets/js/index.js"></script>

<script src="assets/js/data-table/jquery.dataTables.min.js"></script>
<script src="assets/js/data-table/dataTables.bootstrap4.min.js"></script>


</body>

<!-- Mirrored from html.codedthemes.com/mash-able/light/landingpage/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 12 Jun 2019 18:23:20 GMT -->
</html>
