function login(){
  var loginVO = new Object();
  loginVO.username = $("#user-name").val();
  loginVO.password = $("#password").val();
  $.ajax({
    url: host+"/login.php",
    data:JSON.stringify(loginVO),
    type:'POST',
    dataType : 'json',
    contentType : 'application/json;charset=utf-8',
    success:function(data){
      console.log(data);
      if(data.error_code == 'success')
        localStorage.setItem('logintype',data.designation);
      if(data.designation == 'user')
        window.location.href='calculate_salary.php';
      else
        window.location.href='structure.php';
    },
    error:function(e){
      toastr.error("Error in saving structure values",{timeout:5000});
    }
  });
}
