$(document).ready(function(){
  $("#update_structure").hide();
  $("#edit_fixedVal").hide();
  $("#updateFixed_val").hide();
  $("#revised_month_div").hide();
  $("#salary_check").click(function(){
    if($("#salary_check").is(":checked")){
      $("#revised_month_div").fadeIn();
    }
    else
      $("#revised_month_div").fadeOut();
  });
  getFixedValues();
  getFullData();
});
function isEmpty(val) {
  return (val === undefined || val == null || val == "null" || val.length <= 0) ? true : false;
}
function getFixedValues(){
  $.ajax({
    url:"http://localhost/landingpage/onload_data.php",
    type:'POST',
    dataType : 'json',
    contentType : 'application/json;charset=utf-8',
    success:function(data){
      console.log(data);
      if(data.count ==  0){
       var saved_row ='<tr>';
       saved_row += '<td><input type="text" class="form-control" placeholder="Enter ESIC" id="fixed-esic" name="esic_no"></td>';
       saved_row += '<td><input type="text" class="form-control" placeholder="Enter ESIC" id="fixed-esic-2" name="esic_no_two"></td>';
       saved_row += '<td><input type="text" class="form-control" placeholder="Enter PF (12%)" id="fixed-pf-12" name="pf1"></td>';
       saved_row += '<td><input type="text" class="form-control" placeholder="Enter PF (8.33%)" id="fixed-pf-8" name="pf2"></td>';
       saved_row += '<td><input type="text" class="form-control" placeholder="Enter PF (3.67%)" id="fixed-pf-3" name="pf3"></td>';
       saved_row += '<td class="change-update"><button class="btn btn-success btn-mini" type="button" id="save_btn" onclick=save_updateFixedValues("Save",0)>Save</button></td></tr>';
     }
     else{
      var saved_row ='<tr>';
      saved_row += '<td class="edit_true" id="fixed-esic">'+data.esic+'</td>';
      saved_row += '<td class="edit_true" id="fixed-esic-2">'+data.esic_two+'</td>';
      saved_row += '<td contenteditable="false" class="edit_true" id="fixed-pf-12">'+data.fixed_pf_one+'</td>';
      saved_row += '<td contenteditable="false" class="edit_true" id="fixed-pf-8" onchange="getPfThree()">'+data.fixed_pf_two+'</td>';
      saved_row += '<td contenteditable="false" class="edit_true" id="fixed-pf-3">'+data.fixed_pf_three+'</td>';
        // if(!isEmpty(data.esic) || !isEmpty(data.fixed_pf_one) || !isEmpty(data.fixed_pf_two) || !isEmpty(data.fixed_pf_three))
        saved_row += '<td class="change-update"><button class="btn btn-warning btn-mini" type="button" id="edit_fixedVal" onclick=editFixedValues('+data.pf_id+',)>Edit</button></td></tr>';    
      }
      $("#fixed_values_table tbody").html(saved_row);
    },
    error:function(e){
            // console.log(e);
          }
        });
}

function getHRA(){
  var basic = parseInt($("#basic").val())
  var da = parseInt($("#da").val());
  var total_hra = Math.round(((basic+da)*5)/100);
  var hra = $("#hra").val(total_hra);
}
function editFixedValues(pfId){
  $(".edit_true").css("background","lightgray");
  $(".edit_true").attr('contenteditable','true');
  $(".change-update").html('<button class="btn btn-success btn-mini" id="update_btn" type="button" onclick=save_updateFixedValues("Edit",'+pfId+')>Update</button>');
}

function save_updateFixedValues(type,pfid){
  if(type == 'Save'){
    var saveVo = new Object();
    saveVo.esic = $("#fixed-esic").val();
    saveVo.esic_two = $("#fixed-esic-2").val();
    saveVo.pf1 = $("#fixed-pf-12").val();
    saveVo.pf2 = $("#fixed-pf-8").val();
    saveVo.pf3 = $("#fixed-pf-3").val();
    saveVo.value = type;
    saveVo.pf_id = pfid;
  }
  else{
    var saveVo = new Object();
    saveVo.esic = $("#fixed-esic").text();
    saveVo.esic_two = $("#fixed-esic-2").text();
    saveVo.pf1 = $("#fixed-pf-12").text();
    saveVo.pf2 = $("#fixed-pf-8").text();
    saveVo.pf3 = $("#fixed-pf-3").text();
    saveVo.value = type;
    saveVo.pf_id = pfid;
  }
  $.ajax({
    url: "http://localhost/landingpage/submit_pfdata.php",
    data:JSON.stringify(saveVo),
    type:'POST',
    dataType : 'json',
    contentType : 'application/json;charset=utf-8',
    success:function(data){
      console.log(data);
      if(data.status == 'success'){
        if(type == 'Save'){
          alert("Saved Successfully");
        }
        else{
          alert("Updated Successfully");
          $(".edit_true").css("background","transparent");
          $(".edit_true").attr('contenteditable','false');
        }
        getFixedValues();
      }
    },
    error:function(e){
      toastr.error("Error in saving structure values",{timeout:5000});
    }
  });
}

function isNumber(evt) {
  evt = (evt) ? evt : window.event;
  var charCode = (evt.which) ? evt.which : evt.keyCode;
  if (charCode > 31 && (charCode < 48 || charCode > 57)) {
    return false;
  }
  return true;
}
/*    function updateFixedValues(data){
     var esic = $("#fixed-esic").val();
     var pf1 = $("#fixed-pf-12").val();
     var pf2 = $("#fixed-pf-8").val();
     var pf3 = $("#fixed-pf-3").val();
     var value = data;
     var updateVo = {
      esic : esic,
      pf1 : pf1,
      pf2 : pf2,
      pf3 : pf3,
      value : value
    }
    $.ajax({
      url: host+"submit_pfdata.php",
        data:JSON.stringify(updateVo),
        type:'POST',
        dataType : 'json',
        contentType : 'application/json;charset=utf-8',
        success:function(result){
          console.log(result);

        },
        error:function(e){
            // console.log(e);
          }
        });
      }*/
      function getFullData(){
        $.ajax({
          url: "http://localhost/landingpage/pagerefreshdata.php",
          type:'POST',
          dataType: 'json',
          async:false,
          success:function(response){
            console.log(response);
            $("#uan_no").val('');
            $("#esic_no").val('');
            $("#basic").val('');
            $("#gender option:selected").val('');
            $("#emp_name").val('');
            $("#da").val('');
            $("#hra").val('');
            $("#conv").val('');
            $("#medical_allowance").val('');
            $("#personal_pay").val('');
            $("#over_time").val('');
            $("#transport_allowances").val('');
            $("#account_number").val('');
            var structure_row='';
            $.each(response,function(i,obj){
              structure_row +='<tr>';
              structure_row +='<td>'+obj.establishment_name+' ('+obj.est_location+')</td>';
              structure_row +='<td>'+obj.uan_no+'</td>';
              structure_row +='<td>'+obj.esic_no+'</td>';
              structure_row +='<td>'+obj.emp_name+'</td>';
              structure_row +='<td>'+obj.basic+'</td>';
              structure_row +='<td>'+obj.da+'</td>';
              structure_row +='<td>'+obj.hra+'</td>';
              structure_row +='<td>'+obj.conv+'</td>';           
              structure_row +='<td>'+obj.medical_allowance+'</td>';
              structure_row +='<td>'+obj.over_time+'</td>';
              structure_row +='<td>'+obj.personal_pay+'</td>';
              if(obj.pfdeduction_twelve == 1)
                structure_row +='<td>15K</td>';
              else
               structure_row +='<td></td>';
             if(obj.pfdeduction_eight == 1)
              structure_row +='<td>15K</td>';
            else
              structure_row +='<td></td>';
            if(obj.salary_fixedmonth == 1)
              structure_row +='<td>Y</td>';
            else
              structure_row +='<td>N</td>';
            structure_row +='<td>'+obj.account_number+'</td>';
            structure_row +='<td>'+obj.revised_month+'</td>';
            structure_row +='<td><button class="btn btn-warning btn-mini" type="button" id="'+obj.est_location+'_'+obj.emp_name+'" onclick=editEntry('+obj.str_id+',"'+obj.establishment_name+'",this.id,"'+obj.uan_no+'","'+obj.esic_no+'","'+obj.basic+'",'+obj.da+','+obj.hra+','+obj.conv+',"'+obj.medical_allowance+'","'+obj.over_time+'","'+obj.personal_pay+'","'+obj.pfdeduction_twelve+'","'+obj.pfdeduction_eight+'","'+obj.salary_fixedmonth+'","'+obj.account_number+'","'+obj.revised_month+'","'+obj.gender+'")>Edit</button> <button class="btn btn-danger btn-mini" type="button" onclick="deleteEntry('+obj.str_id+')">Delete</button></td>';
          });
            if(structure_row != ''){
              var structure_table = '<table class="table table-bordered" id="structural_table"><thead><tr><th>Establishment Name (Location)</th>'
              structure_table += '<th>UAN No</th>';
              structure_table += '<th>ESIC No</th>';
              structure_table += '<th>Employee Name</th>';            
              structure_table += '<th>Basic</th>';
              structure_table += '<th>DA</th>';
              structure_table += '<th>HRA</th>';
              structure_table += '<th>Conv.</th>';
              structure_table += '<th>Medical Allowance</th>';
              structure_table += '<th>Over Time</th>';
              structure_table += '<th>Personal Pay</th>';
              structure_table += '<th>PF (12%)</th>';
              structure_table += '<th>PF(8.33%)</th>';
              structure_table += '<th>Salary</th>';
              structure_table += '<th>Account Number</th>';
              structure_table += '<th>Revised Month</th><th>Action</th></tr></thead><tbody>';
              structure_table += structure_row;
              structure_table += '</tbody></table>';
            }
            $("#savedList").html(structure_table);
            $("#structural_table").DataTable();
          },
          error:function(e){
            // console.log(e);
          }
        });
}


function submitValue(){
 var uan_no = $("#uan_no").val();
 var esic_no = $("#esic_no").val();
 var basic = $("#basic").val();
 var establishment_name = $("#establishment_name").val();
 var est_location = $("#est_location").val();
 var gender = $("#gender option:selected").val();
 var emp_name = $("#emp_name").val();
 var da = $("#da").val();
 var hra = $("#hra").val();
 var conv = $("#conv").val();
 var medical_allowance = $("#medical_allowance").val();
 var personal_pay = $("#personal_pay").val();
 var over_time = $("#over_time").val();
 var transport_allowances = $("#transport_allowances").val();
 var account_number = $("#account_number").val();
 if($("input[name='pf_salary_12']").is(":checked"))
  var pf_salary_12 = true;
else
  var pf_salary_12 = false;

if($("input[name='pf_salary_8']").is(":checked"))
  var pf_salary_8 = true;
else
  var pf_salary_8 = false;

if($("input[name='salary_month_days']").is(":checked"))
  var salary_month = true;
else
  var salary_month = false;

var revised_month_days = $("#revised_month_days").val();
if(revised_month_days > 31){
  alert("Please provide valid month days");
  return false;
}
if(salary_month == true && revised_month_days == '') {
  alert("Please provide revised month days");
  return false;
}

var salaryVo = {
  uan_no : uan_no,
  esic_no : esic_no,
  basic : basic,
  establishment_name : establishment_name,
  est_location : est_location,
  gender : gender,
  emp_name : emp_name,
  da : da,
  hra : hra,
  conv : conv,
  medical_allowance : medical_allowance,
  personal_pay : personal_pay,
  over_time : over_time,
  account_no : account_number,
  pf_salary_12 : pf_salary_12,
  pf_salary_8 : pf_salary_8,
  salary_month : salary_month,
  revised_month_days : revised_month_days
}

$.ajax({
  url: host+"/structural_data.php",
  data:JSON.stringify(salaryVo),
  type:'POST',
  dataType: 'json',
  async:false,
  success:function(response){
    console.log(response);
    getFullData();
  },
  error:function(e){
            // console.log(e);
          }
        });
}

function editEntry(empId,est_name,location,uan,esic_no,basic,da,hra,conv,medical,overtime,personal,pf12,pf8,salary,acc,revised,gender){
  var loc_empname = location.split("_");
  $("#establishment_name").text(est_name);
  $("#est_location").val(loc_empname[0]);
  $("#uan_no").val(uan);
  $("#esic_no").val(esic_no);
  $("#emp_name").val(loc_empname[1]);
  $("#account_number").val(acc);
  $("#basic").val(basic);
  $("#da").val(da);
  $("#hra").val(hra);
  $("#conv").val(conv);
  $("#medical_allowance").val(medical);
  $("#over_time").val(overtime);
  $("#personal_pay").val(personal);
  getGender(gender);
      // $("#gender option:selected").text(gender);
      $("#emp_id").val(empId);
      if(pf12 == 1)
        $("input[name='pf_salary_12'").prop('checked',true);
      else
        $("input[name='pf_salary_12'").prop('checked',false);
      if(pf8 == 1)
        $("input[name='pf_salary_8'").prop('checked',true);
      else
        $("input[name='pf_salary_8'").prop('checked',false);
      if(salary == 1){
        $("input[name='salary_month_days'").prop('checked',true);
        $("#revised_month_div").show();
        $("#revised_month_days").val(revised);
      }
      else{
        $("input[name='salary_month_days'").prop('checked',false);
        $("#revised_month_div").hide();
        $("#revised_month_days").val('');
      }
      $("#update_structure").show();
      $("#submit_structure").hide();
    }
    function getGender(gender) {
      var caseType = "";
      if (isEmpty(gender)) {
        gender = '';
      } else {
        var objSelect = document.getElementById("gender");
        setSelectedValue(objSelect,gender); 
      }
    }

    function setSelectedValue(selectObj, valueToSet) {
      for (var i = 0; i < selectObj.options.length; i++) {
        if (selectObj.options[i].text== valueToSet) {
          selectObj.options[i].selected = true;
          return;
        }
      }
    }
    function updateEntry(){
      var structuralVo= new Object();
      structuralVo.str_id = parseInt($("#emp_id").val());
      structuralVo.establishment = $("#establishment_name").val();
      structuralVo.est_location =  $("#est_location").val();
      structuralVo.uan_no =  $("#uan_no").val();
      structuralVo.esic_no = $("#esic_no").val();
      structuralVo.emp_name = $("#emp_name").val();
      structuralVo.account_no = $("#account_number").val();
      structuralVo.basic = $("#basic").val();
      structuralVo.da = $("#da").val();
      structuralVo.hra = $("#hra").val();
      structuralVo.conv = $("#conv").val();
      structuralVo.medical = $("#medical_allowance").val();
      structuralVo.over_time = $("#over_time").val();
      structuralVo.personal_pay = $("#personal_pay").val();
      structuralVo.gender = $("#gender option:selected").val();     
      structuralVo.pfdeduction_twelve = $("input[name='pf_salary_12'").prop('checked') == true ? 1 : 0;
      structuralVo.pfdeduction_eight =  $("input[name='pf_salary_8'").prop('checked') == true ? 1 : 0;
      structuralVo.salary_fixedmonth = $("input[name='salary_month_days'").prop('checked') == true ? 1 : 0;
      structuralVo.revised_month = $("#revised_month_days").val();

      $.ajax({
        url: "http://localhost/landingpage/update_structuredata.php",
        data:JSON.stringify(structuralVo),
        type:'POST',
        dataType : 'json',
        contentType : 'application/json;charset=utf-8',
        success:function(data){
          console.log(data);
          if(data.status == 'success'){
            getFullData();
            $("#update_structure").hide();
            $("#submit_structure").show();
          }
          
        },
        error:function(e){
            // console.log(e);
          }
        });
    }

    function deleteEntry(empId){
      var empVo={
        str_id : empId
      }
      $.ajax({
        url: "http://localhost/landingpage/delete_structuredata.php",
        data:JSON.stringify(empVo),
        type:'POST',
        dataType : 'json',
        contentType : 'application/json;charset=utf-8',
        success:function(data){
          console.log(data);
          getFullData();
        },
        error:function(e){
            // console.log(e);
          }
        });
    }